import { Component, OnInit } from '@angular/core';
import { DriverTariffService as Service} from "../../_services/index";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-driver-tariff',
  templateUrl: './driver-tariff.component.html',
  styleUrls: ['./driver-tariff.component.css']
})
export class DriverTariffComponent implements OnInit {

    model : any = {};
    errors : any = [];
    succeed : boolean;
    constructor(private service: Service, private route:ActivatedRoute, private router: Router) {
      route.data
      .subscribe(data => {
        console.log(data);
        this.model = data.data;
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
    }

    submitted = false;

    onSubmit() {
      this.succeed = null;
      this.errors = [];
  		this.service.update(this.model).subscribe(data => {
  			this.succeed = true;
        this.errors = [];
  		},
  		error => {
  			this.succeed = false;
  			this.errors = error['error'];
  		});
		console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
