import {Component, OnInit} from '@angular/core';
import {DriverService as Service} from '../../_services/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Driver} from '../../_models/driver';

@Component({
  selector: 'app-driver-edit',
  templateUrl: './driver-edit.component.html',
  styleUrls: ['./driver-edit.component.css']
})
export class DriverEditComponent implements OnInit {

  model: any = new Driver();
  errors: any = [];
  uploaded = false;
  succeed: boolean;
  src = '';

  constructor(private service: Service, private route: ActivatedRoute, private router: Router) {
    route.data
      .subscribe(data => {
          this.model = Object.assign(this.model, data.data);
        },
        error => {
          console.log(error['error']);
          this.errors = error['error'];
        });
  }

  getImageSource() {
    if (!this.uploaded)
      return this.model.getImagePath();
    else
      return this.src;
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = (e: any) => {
        this.uploaded = true;
        this.src = e.target.result;
        this.model.file = reader.result.split(',')[1];
      };
    }
  }

  submitted = false;

  onSubmit() {
    this.succeed = null;
    this.errors = [];
    this.service.update(this.model).subscribe(data => {
        this.succeed = true;
        // this.router.navigate(['drivers']);
      },
      error => {
        this.succeed = false;
        this.errors = error['error'];
      });
  }

  ngOnInit() {

  }

  hasError(field) {
    let classList = {
      'has-error': this.errors[field]
    };
    return classList;
  }
}
