import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DriverCarCreateComponent } from './driver-car-create.component';

describe('DriverCarCreateComponent', () => {
  let component: DriverCarCreateComponent;
  let fixture: ComponentFixture<DriverCarCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DriverCarCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DriverCarCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
