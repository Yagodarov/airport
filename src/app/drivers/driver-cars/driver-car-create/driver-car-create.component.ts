import { Component, OnInit } from '@angular/core';
import { DriverCarService as Service} from "../../../_services/index";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-driver-car-create',
  templateUrl: './driver-car-create.component.html',
  styleUrls: ['./driver-car-create.component.css']
})
export class DriverCarCreateComponent implements OnInit {

    model : any = {};
    errors : any = {};
    cars : any = [];
    constructor(private service: Service,private router: Router, private route:ActivatedRoute) {
    	this.model.driver_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
    	route.data.subscribe(
    		cars => {
    			this.cars = cars['cars']['models'];
    		});
    }

    submitted = false;

    onSubmit() {
      this.service.create(this.model).subscribe(data => {
        this.router.navigate(['/driver/'+this.model.driver_id+'/cars']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.image = reader.result.split(',')[1];
      };
    }
  }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
