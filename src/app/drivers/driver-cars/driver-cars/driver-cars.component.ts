import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DriverCarService as Service} from "../../../_services/index";

@Component({
  selector: 'app-driver-cars',
  templateUrl: './driver-cars.component.html',
  styleUrls: ['./driver-cars.component.css']
})
export class DriverCarsComponent implements OnInit {
	pager;
	sort: string = "id";
	driver_id : number;
	desc: boolean = true;
	search = [];
	columns = [
		{ name: 'id' , descr: '#' },
		{ name: 'mifare_card_id' , descr: 'Mifare card id' },
		{ name: 'car_id' , descr: 'авто' },
	];
	models:any;
	count:any;
	page:any=1;
	constructor(private route:ActivatedRoute, private service: Service) {
		this.driver_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
	}

	ngOnInit() {
		this.route.data
				.subscribe(data => {
					console.log(data);
					this.models = data['data']['models'];
					this.count = data['data']['count'];
					this.page = 1;
				});
	}

	delete(id: number) {
		if (confirm("Точно удалить?"))
			this.service.delete(id).subscribe(() => { this.loadModels() });
	}

	setPage(page){
		this.page = page;
		this.loadModels();
	}

	setSort(data){
		if (this.sort == data)
			this.desc = !this.desc;
		else
			this.desc = true;
		this.sort = data;
		this.loadModels();
	}

	loadModels() {
		this.service.getAll(this.getParams()).subscribe(data => {
			this.models = data['models'];
		});
	}

	inputSearch()
    {
        this.page = 1;
        this.loadModels();
    }

	getParams() {
		var params = {
            page:String(this.page - 1),
            orderBy:this.sort,
            desc:this.desc,
        };
		var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key+' '+value);
            params[key] = value;
        };
        params['driver_id'] = this.driver_id;
		return {params:params};
	}
}
