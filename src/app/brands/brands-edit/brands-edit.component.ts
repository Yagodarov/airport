import { Component, OnInit } from '@angular/core';
import { BrandService as Service} from "../../_services/index";
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-brands-edit',
  templateUrl: './brands-edit.component.html',
  styleUrls: ['./brands-edit.component.css']
})
export class BrandsEditComponent implements OnInit {
    uploaded = false;
    src = '';
    model : any = {};
    errors : any = [];
    constructor(private service: Service, private route:ActivatedRoute, private router: Router) {
      route.data
      .subscribe(data => {
        console.log(data);
        this.model = data.data;
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
    }

    submitted = false;

    onSubmit() { 
      this.service.update(this.model).subscribe(data => {
        this.router.navigate(['brands']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    onFileChange(event) {
      let reader = new FileReader();
      if(event.target.files && event.target.files.length > 0) {
        let file = event.target.files[0];
        reader.readAsDataURL(file);
        reader.onload = (e:any) => {
          this.uploaded = true;
          this.src = e.target.result;
          this.model.file = reader.result.split(',')[1];
        };
      }
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
