import { Component, OnInit } from '@angular/core';
import { ClientTypeService as Service} from "../../_services/index";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-client-type-edit',
  templateUrl: './client-type-edit.component.html',
  styleUrls: ['./client-type-edit.component.css']
})
export class ClientTypeEditComponent implements OnInit {

    model : any = {};
    errors : any = [];
    constructor(private service: Service, private route:ActivatedRoute, private router: Router) {
      route.data
      .subscribe(data => {
        this.model = data.data;
      },
      error => {
        this.errors = error['error'];
      });
    }
    onSubmit() { 
      this.service.update(this.model).subscribe(data => {
        this.router.navigate(['client_types']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
