import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TariffEditComponent } from './tariff-edit.component';

describe('TariffEditComponent', () => {
  let component: TariffEditComponent;
  let fixture: ComponentFixture<TariffEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TariffEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TariffEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
