import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
    selector: 'child-cell',
    template: `<span><button style="height: 20px" (click)="invokeParentMethod()" class="btn btn-info">Редактировать</button></span>`,
    styles: [
        `.btn {
            line-height: 0.5
        }`
    ]
})
export class EditButtonRenderer{
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    constructor(private route: Router)
    {

    }

    public invokeParentMethod() {
        this.route.navigate([this.route.url+'/edit/'+this.params.data.id]);
        // this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    }

    refresh(): boolean {
        return false;
    }
}