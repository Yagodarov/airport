import {BrowserModule} from '@angular/platform-browser';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS, HttpHandler} from '@angular/common/http';
import {SafeHtmlPipe} from './_helpers/safe.html';
import {AlertComponent} from './_directives';
import {AuthGuard} from './_guards';
import {JwtInterceptor, ErrorInterceptor} from './_helpers';
import {
  AlertService, AuthenticationService, UserService, DriverService, CarBrandService, CarImageService, BrandService, Parking,
  TariffService, ClientTypeService, DriverTariffService, DriverCarService, CarService, AreaService, OrderService, PrintService,
  TerminalService, OptionsService, SmsService, ActionsUserService, RequestService, AuctionService, ParticipantsService,
} from './_services';
import {AppComponent} from './app.component';
import {routing} from './app.routing';
import {LoginComponent} from './login/login.component';
import {NavComponent} from './nav/nav.component';
import {Pager} from './pager/pager.component';
import {
  UsersResolver, UserEditResolver, DriversResolver, DriverEditResolver, CarBrandsResolver, CarCreateResolver,
  BrandsResolver, CarBrandEditResolver, BrandEditResolver, TariffsResolver, TariffEditResolver, ClientTypesResolver,
  ClientTypeEditResolver, DriverTariffEditResolver, DriverCarsResolver, CarsResolver, CarEditResolver,
  CarsListResolver, DriverCarEditResolver, CarDriversResolver, DriversListResolver, CarImagesResolver, CarImageEditResolver,
  ParkingResolver, UsersListResolver, AreasResolver, OrdersResolver, OrderResolver,
  TerminalResolver, OptionsResolver, SmsResolver, ParticipantsResolver
} from './_resolvers';
import {UsersComponent} from './users/users/users.component';
import {UserCreateComponent} from './users/user-create/user-create.component';
import {UserEditComponent} from './users/user-edit/user-edit.component';
import {Ripple} from './_directives/ripple';
import {DriversComponent} from './drivers/drivers/drivers.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {DriverCreateComponent} from './drivers/driver-create/driver-create.component';
import {DriverEditComponent} from './drivers/driver-edit/driver-edit.component';
import {EditButtonRenderer} from './_extends/childmessage';
import {CarBrandsComponent} from './car-brands/car-brands/car-brands.component';
import {BrandsComponent} from './brands/brands/brands.component';
import {CarBrandsCreateComponent} from './car-brands/car-brands-create/car-brands-create.component';
import {CarBrandsEditComponent} from './car-brands/car-brands-edit/car-brands-edit.component';
import {BrandsCreateComponent} from './brands/brands-create/brands-create.component';
import {BrandsEditComponent} from './brands/brands-edit/brands-edit.component';
import {TariffsComponent} from './tariffs/tariffs/tariffs.component';
import {TariffCreateComponent} from './tariffs/tariff-create/tariff-create.component';
import {TariffEditComponent} from './tariffs/tariff-edit/tariff-edit.component';
import {ClientTypesComponent} from './client-types/client-types/client-types.component';
import {ClientTypeCreateComponent} from './client-types/client-type-create/client-type-create.component';
import {ClientTypeEditComponent} from './client-types/client-type-edit/client-type-edit.component';
import {DriverNavigationComponent} from './drivers/driver-navigation/driver-navigation.component';
import {DriverTariffComponent} from './drivers/driver-tariff/driver-tariff.component';
import {DriverCarsComponent} from './drivers/driver-cars/driver-cars/driver-cars.component';
import {CarsComponent} from './cars/cars/cars.component';
import {CarEditComponent} from './cars/car-edit/car-edit.component';
import {CarCreateComponent} from './cars/car-create/car-create.component';
import {CarNavigationComponent} from './cars/car-navigation/car-navigation.component';
import {CarImagesComponent} from './cars/car-images/car-images/car-images.component';
import {CarDriversComponent} from './cars/car-drivers/car-drivers/car-drivers.component';
import {DriverCarCreateComponent} from './drivers/driver-cars/driver-car-create/driver-car-create.component';
import {DriverCarEditComponent} from './drivers/driver-cars/driver-car-edit/driver-car-edit.component';
import {DriverCarWrapperComponent} from './drivers/driver-cars/driver-car-wrapper/driver-car-wrapper.component';
import {ParkingComponent} from './parking/parking/parking.component';
import {ParkingsComponent} from './parking/parkings/parkings.component';
import {ParkingEntryComponent} from './parking/parking-entry/parking-entry.component';
import {ParkingExitComponent} from './parking/parking-exit/parking-exit.component';
import {CarDriversWrapperComponent} from './cars/car-drivers/car-drivers-wrapper/car-drivers-wrapper.component';
import {CarDriverCreateComponent} from './cars/car-drivers/car-driver-create/car-driver-create.component';
import {CarDriverEditComponent} from './cars/car-drivers/car-driver-edit/car-driver-edit.component';
import {CarImagesWrapperComponent} from './cars/car-images/car-images-wrapper/car-images-wrapper.component';
import {CarImagesCreateComponent} from './cars/car-images/car-images-create/car-images-create.component';
import {CarImagesEditComponent} from './cars/car-images/car-images-edit/car-images-edit.component';
import {MainMenuComponent} from './main/main-menu/main-menu.component';
import {TerminalComponent} from './terminal/terminal/terminal.component';
import {TerminalStepsComponent} from './terminal/terminal-steps/terminal-steps.component';
import {TerminalModalComponent} from './terminal/terminal-modal/terminal-modal.component';
import {MapZonesComponent} from './maps/map-zones/map-zones.component';
import {TerminalPrintComponent} from './terminal/terminal-print/terminal-print.component';
import {OrdersComponent} from './orders/orders/orders.component';
import {OrderViewComponent} from './orders/order-view/order-view.component';
import {OptionsComponent} from './options/options.component';
import { SmsComponent } from './sms/sms/sms.component';
import { ActionsUserComponent } from './actions/actions-user/actions-user.component';
import {ActionsUserResolver} from './_resolvers/actions_user';
import { TaxiOrderedComponent } from './actions/taxi-ordered/taxi-ordered.component';
import { CarOrderedByUserComponent } from './actions/car-ordered-by-user/car-ordered-by-user.component';
import { PhoneRegisterRequestsComponent } from './mobile/phone-register-requests/phone-register-requests.component';
import { AuctionsComponent } from './auction/auctions/auctions.component';
import { AuctionCreateComponent } from './auction/auction-create/auction-create.component';
import { AuctionEditComponent } from './auction/auction-edit/auction-edit.component';
import { AuctionParticipantsComponent } from './auction/auction-participants/auction-participants.component';

@NgModule({
    declarations: [

        AlertComponent,
        AppComponent,
        SafeHtmlPipe,
        LoginComponent,
        NavComponent,
        UsersComponent,
        Pager,
        UserCreateComponent,
        UserEditComponent,
        Ripple,
        DriversComponent,
        NotFoundComponent,
        DriverCreateComponent,
        DriverEditComponent,
        EditButtonRenderer,
        CarBrandsComponent,
        BrandsComponent,
        CarBrandsCreateComponent,
        CarBrandsEditComponent,
        BrandsCreateComponent,
        BrandsEditComponent,
        TariffsComponent,
        TariffCreateComponent,
        TariffEditComponent,
        ClientTypesComponent,
        ClientTypeCreateComponent,
        ClientTypeEditComponent,
        DriverNavigationComponent,
        DriverTariffComponent,
        DriverCarsComponent,
        CarsComponent,
        CarEditComponent,
        CarCreateComponent,
        CarNavigationComponent,
        CarImagesComponent,
        CarDriversComponent,
        DriverCarCreateComponent,
        DriverCarEditComponent,
        ParkingComponent,
        DriverCarWrapperComponent,
        ParkingsComponent,
        ParkingEntryComponent,
        ParkingExitComponent,
        CarDriversWrapperComponent,
        CarDriverCreateComponent,
        CarDriverEditComponent,
        CarImagesWrapperComponent,
        CarImagesCreateComponent,
        CarImagesEditComponent,
        MainMenuComponent,
        TerminalComponent,
        TerminalStepsComponent,
        TerminalModalComponent,
        MapZonesComponent,
        TerminalPrintComponent,
        OrdersComponent,
        OrderViewComponent,
        OptionsComponent,
        SmsComponent,
        ActionsUserComponent,
        TaxiOrderedComponent,
        CarOrderedByUserComponent,
        PhoneRegisterRequestsComponent,
        AuctionsComponent,
        AuctionCreateComponent,
        AuctionEditComponent,
        AuctionParticipantsComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatButtonModule, MatCheckboxModule, MatButtonModule,
        ReactiveFormsModule,
        routing,
    ],
    providers: [
        AuthGuard,
        AlertService, AuthenticationService, UserService, DriverService, DriverTariffService, DriverCarService,
        CarBrandService, BrandService, UserService, TariffService, ClientTypeService, CarService,
        CarImageService, Parking, AreaService, OrderService, PrintService, RequestService,
        TerminalService, OptionsService, SmsService, ActionsUserService, AuctionService, ParticipantsService,
        UsersResolver, UserEditResolver, DriversResolver, DriverEditResolver, ClientTypesResolver, CarCreateResolver,
        CarBrandsResolver, BrandsResolver, CarBrandEditResolver, BrandEditResolver, TariffsResolver, ClientTypeEditResolver,
        TariffEditResolver, DriverTariffEditResolver, DriverCarsResolver, CarsResolver, CarEditResolver,
        CarsListResolver, DriverCarEditResolver, CarDriversResolver, DriversListResolver, CarImagesResolver, CarImageEditResolver,
        ParkingResolver, UsersListResolver, AreasResolver, OrdersResolver, OrderResolver, SmsResolver, ActionsUserResolver,
        TerminalResolver, OptionsResolver,ParticipantsResolver,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        },
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
