import {Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {UserService as Service} from '../../_services/user.service';
import {UserHelper} from '../../_helpers/user.helper';
import {User as Model} from '../../_models/user';

@Component({
  moduleId: module.id.toString(),
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  providers: [UserHelper]
})
export class UsersComponent implements OnInit {
  pager;
  sort: string = 'id';
  desc: boolean = true;
  search = [];
  model = new Model();
  roles: any = {};
  columns = [
    {name: 'id', descr: '#'},
    {name: 'name', descr: 'ФИО'},
    {name: 'username', descr: 'Логин'},
    {name: 'role', descr: 'Роль'},
    {name: 'blocked', descr: 'Блокировка'}
  ];
  models: any;
  count: any;
  page: any = 1;

  constructor(private route: ActivatedRoute, private service: Service) {
    this.roles = this.model.getRoles();
  }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        console.log(data);
        this.models = data['data']['models'];
        this.count = data['data']['count'];
        this.page = 1;
      });
  }

  toggleBlock(model) {
    let update: any = Object.assign({}, model);
    update.blocked = update.blocked == 1 ? 0 : 1;
    this.service.block(update).subscribe(() => {
      model.blocked = model.blocked == 1 ? 0 : 1;
    });
  }

  delete(id: number) {
    if (confirm('Точно удалить?'))
      this.service.delete(id).subscribe(() => {
        this.loadModels();
      });
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort == data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  getUserFullName(item) {
    return item['surname'] + ' ' + item['first_name'];
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }
}
