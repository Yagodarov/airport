import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarImagesCreateComponent } from './car-images-create.component';

describe('CarImagesCreateComponent', () => {
  let component: CarImagesCreateComponent;
  let fixture: ComponentFixture<CarImagesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarImagesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarImagesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
