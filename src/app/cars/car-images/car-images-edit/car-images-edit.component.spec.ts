import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarImagesEditComponent } from './car-images-edit.component';

describe('CarImagesEditComponent', () => {
  let component: CarImagesEditComponent;
  let fixture: ComponentFixture<CarImagesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarImagesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarImagesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
