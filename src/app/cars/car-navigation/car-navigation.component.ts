import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-car-navigation',
  templateUrl: './car-navigation.component.html',
  styleUrls: ['./car-navigation.component.css']
})
export class CarNavigationComponent implements OnInit {
  id : any;
  constructor(private route: ActivatedRoute) {
    this.id = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {

  }

}
