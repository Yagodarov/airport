import {Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {CarService as Service} from '../../_services/index';
import {UserHelper} from '../../_helpers/user.helper';
import {Car as Model} from '../../_models/car';

declare var $: any;

@Component({
    selector: 'app-cars',
    templateUrl: './cars.component.html',
    styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
    pager;
    sort: string = 'id';
    desc: boolean = true;
    search = [];
    model = new Model();
    roles: any = {};
    columns = [
        {name: 'id', descr: '#'},
        {name: 'mark', descr: 'Автомобиль'},
        {name: 'is_black_list', descr: 'Черный список'},
        {name: 'permission_to_drive', descr: 'Статус'},
    ];
    models: any;
    count: any;
    page: any = 1;

    constructor(private route: ActivatedRoute, private service: Service) {

    }

    toggleBlock(model) {
        let update: any = Object.assign({}, model);
        update.is_black_list = update.is_black_list == 1 ? 0 : 1;
        this.service.blackList(update).subscribe(() => {
            model.is_black_list = model.is_black_list == 1 ? 0 : 1;
        });
    }

    togglePermission(model) {
        let update: any = Object.assign({}, model);
        update.permission_to_drive = update.permission_to_drive == 1 ? 0 : 1;
        this.service.permission(update).subscribe(() => {
            model.permission_to_drive = model.permission_to_drive == 1 ? 0 : 1;
        });
    }

    ngOnInit() {
        this.route.data
            .subscribe(data => {
                console.log(data);
                this.models = data['data']['models'];
                this.count = data['data']['count'];
                this.page = 1;
            });
    }

    delete(id: number) {
        if (confirm('Точно удалить?'))
            this.service.delete(id).subscribe(() => {
                this.loadModels();
            });
    }

    setPage(page) {
        this.page = page;
        this.loadModels();
    }

    setSort(data) {
        if (this.sort == data)
            this.desc = !this.desc;
        else
            this.desc = true;
        this.sort = data;
        this.loadModels();
    }

    loadModels() {
        this.service.getAll(this.getParams()).subscribe(data => {
            this.models = data['models'];
            this.count = data['count'];
        });
    }

    inputSearch() {
        this.page = 1;
        this.loadModels();
    }

    getParams() {
        var params = {
            page: String(this.page - 1),
            orderBy: this.sort,
            desc: this.desc,
        };
        var search = this.search;
        for (let key in search) {
            let value = search[key];
            console.log(key + ' ' + value);
            params[key] = value;
        }
        return {params: params};
    }
}
