import { Component, OnInit } from '@angular/core';
import { DriverCarService as Service} from "../../../_services/index";
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-car-driver-edit',
  templateUrl: './car-driver-edit.component.html',
  styleUrls: ['./car-driver-edit.component.css']
})
export class CarDriverEditComponent implements OnInit {

    model : any = {};
    errors : any = {};
    drivers : any = [];
    constructor(private service: Service,private router: Router, private route:ActivatedRoute) {
    	this.model.car_id = Number(route.snapshot['_urlSegment']['segments'][1]['path']);
    	route.data.subscribe(
    		data => {
    			console.log(data);
    			this.drivers = data['drivers']['models'];
    			this.model = data['data'];
    		});
    }

    submitted = false;

    onSubmit() {
      this.service.update(this.model).subscribe(data => {
        this.router.navigate(['/cars/'+this.model.car_id+'/drivers']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.image = reader.result.split(',')[1];
      };
    }
  }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
