import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriversComponent } from './car-drivers.component';

describe('CarDriversComponent', () => {
  let component: CarDriversComponent;
  let fixture: ComponentFixture<CarDriversComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriversComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriversComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
