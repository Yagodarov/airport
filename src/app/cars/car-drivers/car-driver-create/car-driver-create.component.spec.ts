import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriverCreateComponent } from './car-driver-create.component';

describe('CarDriverCreateComponent', () => {
  let component: CarDriverCreateComponent;
  let fixture: ComponentFixture<CarDriverCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriverCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriverCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
