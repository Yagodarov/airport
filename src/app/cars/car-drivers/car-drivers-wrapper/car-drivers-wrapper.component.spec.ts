import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarDriversWrapperComponent } from './car-drivers-wrapper.component';

describe('CarDriversWrapperComponent', () => {
  let component: CarDriversWrapperComponent;
  let fixture: ComponentFixture<CarDriversWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarDriversWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarDriversWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
