import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {CarService as Service} from '../../_services/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Car} from '../../_models/car';

@Component({
    moduleId: module.id.toString(),
    templateUrl: './car-edit.component.html',
    styleUrls: ['./car-edit.component.css']
})
export class CarEditComponent implements OnInit {
    uploaded = false;
    src = '';
    model: any = new Car();
    car_brands: any = [];
    car_classes: any = [];
    taxi_brands: any = [];
    client_types: any = [];
    tariffs: any = [];
    errors: any = {};
    succeed: boolean;

    constructor(private service: Service, private router: Router, private route: ActivatedRoute, public cd: ChangeDetectorRef) {
    }

    submitted = false;

    onSubmit() {
        this.succeed = null;
        this.errors = [];
        this.service.update(this.model).subscribe(data => {
                this.succeed = true;

            },
            error => {
                this.succeed = false;
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    getImageSource() {
        if (!this.uploaded)
            return this.model.getImagePath();
        else
            return this.src;
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e: any) => {
                this.uploaded = true;
                this.src = e.target.result;
                this.model.file = reader.result.split(',')[1];
            };
        }
    }

    ngOnInit() {
        this.route.data
            .subscribe(data => {
                    Object.assign(this.model, data['data']);
                    const params = data['params'];
                    setTimeout(() => {
                        this.car_brands = params['car_brands'];
                        this.car_classes = params['car_classes'];
                        this.taxi_brands = params['taxi_brands'];
                        this.client_types = params['client_types'];
                        this.tariffs = params['group_tariffs'];
                    }, 50)
                    this.cd.detectChanges();
                },
                error => {
                    this.errors = error['error'];
                });
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }
}
