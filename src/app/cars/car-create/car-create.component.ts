import {Component, OnInit} from '@angular/core';
import {CarService as Service} from '../../_services/index';
import {ActivatedRoute, Router} from '@angular/router';
import {Car} from '../../_models/car';

declare var $: any;

@Component({
    selector: 'app-car-create',
    templateUrl: './car-create.component.html',
    styleUrls: ['./car-create.component.css']
})
export class CarCreateComponent implements OnInit {

    model: any = new Car();
    car_brands: any = [];
    car_classes: any = [];
    taxi_brands: any = [];
    client_types: any = [];
    tariffs: any = [];
    errors: any = {};

    constructor(private service: Service, private router: Router, private route: ActivatedRoute) {

    }

    submitted = false;

    onSubmit() {
        this.service.create(this.model).subscribe(data => {
                this.router.navigate(['cars/' + data['id'] + '/edit']);
            },
            error => {
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = (e: any) => {
                this.model.file = reader.result.split(',')[1];
                $('#car_img')
                    .attr('src', e.target.result)
                    .width(250);
            };
        }
    }

    ngOnInit() {
        console.log(this.model);
        this.route.data
            .subscribe(data => {
                    data = data['data'];
                    this.car_brands = data['car_brands'];
                    this.car_classes = data['car_classes'];
                    this.taxi_brands = data['taxi_brands'];
                    this.client_types = data['client_types'];
                    this.tariffs = data['group_tariffs'];
                    console.log(data);
                },
                error => {
                    console.log(error['error']);
                    this.errors = error['error'];
                });
    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }
}
