import { Component, OnInit } from '@angular/core';
import { CarBrandService as Service} from "../../_services/index";
import { Router } from '@angular/router';
@Component({
  selector: 'app-car-brands-create',
  templateUrl: './car-brands-create.component.html',
  styleUrls: ['./car-brands-create.component.css']
})
export class CarBrandsCreateComponent implements OnInit {

    model : any = {};
    errors : any = [];
    constructor(private service: Service,private router: Router) { }

    submitted = false;

    onSubmit() { 
      this.service.create(this.model).subscribe(data => {
        this.router.navigate(['car_brands']);
      },
      error => {
        console.log(error['error']);
        this.errors = error['error'];
      });
      console.log(this.model);
    }

    onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.model.image = reader.result.split(',')[1];
      };
    }
  }

    ngOnInit() {

    }

    hasError(field)
    {
        let classList = {
            'has-error' : this.errors[field]
        };
        return classList;
    }
}
