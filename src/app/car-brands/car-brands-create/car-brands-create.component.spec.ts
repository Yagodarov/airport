import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarBrandsCreateComponent } from './car-brands-create.component';

describe('CarBrandsCreateComponent', () => {
  let component: CarBrandsCreateComponent;
  let fixture: ComponentFixture<CarBrandsCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarBrandsCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarBrandsCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
