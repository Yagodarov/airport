import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarBrandsEditComponent } from './car-brands-edit.component';

describe('CarBrandsEditComponent', () => {
  let component: CarBrandsEditComponent;
  let fixture: ComponentFixture<CarBrandsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarBrandsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarBrandsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
