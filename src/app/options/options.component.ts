import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {OptionsService} from '../_services';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit {

    model: any = {};
    user = {};
    errors: any = [];
    roles: any = {};
    objectKeys = Object.keys;
    succeed : boolean;
    constructor(private service: OptionsService, private route: ActivatedRoute, private router: Router) {
        route.data
            .subscribe(data => {
                    console.log(data);
                    this.model = data.data;
                },
                error => {
                    console.log(error['error']);
                    this.errors = error['error'];
                });
    }

    submitted = false;

    onSubmit() {
        this.service.update(this.model).subscribe(data => {
                this.succeed = true;

            },
            error => {
                this.succeed = false;
                this.errors = error['error'];
            });
        console.log(this.model);
    }

    ngOnInit() {

    }

    hasError(field) {
        let classList = {
            'has-error': this.errors[field]
        };
        return classList;
    }


}
