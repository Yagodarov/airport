import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {OrderService as Service} from '../../_services/index';

declare var $: any;
declare var moment: any;

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  pager;
  sort: string = 'id';
  desc: boolean = true;
  search = {
    'action_id': ''
  };
  models: any;
  count: any;
  page: any = 1;
  columns = [
    {descr: 'Фото'},
    {descr: 'Марка, модель'},
    {descr: 'Бренд'},
    {descr: ''},
  ];

  constructor(private route: ActivatedRoute, private service: Service) {
  }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        console.log(data);
        this.models = data['data']['models'];
        this.count = data['data']['count'];
        this.page = 1;
      });
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (data) {
      if (this.sort == data)
        this.desc = !this.desc;
      else
        this.desc = true;
      this.sort = data;
      this.loadModels();
    }
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }

  getTrueDate(date) {
    return moment.utc(date).toISOString();
  }
}
