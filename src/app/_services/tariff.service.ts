import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class TariffService {
    name = 'tariff';
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/'+this.name+'s', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/'+this.name+'/' + id);
    }

    create(model: any) {
        return this.http.post('/api/'+this.name, model);
    }

    update(model: any) {
        return this.http.put('/api/'+this.name+'/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/'+this.name+'/' + id);
    }
}