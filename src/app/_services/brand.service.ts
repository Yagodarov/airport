import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class BrandService {
    constructor(private http: HttpClient) { }

    getAll(params = {}) {
        return this.http.get<any[]>('/api/brands', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/brand/' + id);
    }

    create(model: any) {
        return this.http.post('/api/brand', model);
    }

    update(model: any) {
        return this.http.put('/api/brand/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/brand/' + id);
    }
}