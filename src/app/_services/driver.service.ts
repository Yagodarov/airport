import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class DriverService {
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/drivers', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/driver/' + id);
    }

    create(model: any) {
        return this.http.post('/api/driver', model);
    }

    update(model: any) {
        return this.http.put('/api/driver/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/driver/' + id);
    }
}