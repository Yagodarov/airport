import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class PrintService {
    constructor(private http: HttpClient) { }

    print(model: any) {
        return this.http.post('http://localhost:3377', model);
    }
}