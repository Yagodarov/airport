import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CarService {
    name = 'car';
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/'+this.name+'s', params);
    }

    orderSearch(params= {}) {
        return this.http.get<any[]>('/api/cars/order_search', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/'+this.name+'/' + id);
    }

    getCarParams()
    {
        return this.http.get<any>('/api/'+this.name+'_params');
    }

    create(model: any) {
        return this.http.post('/api/'+this.name, model);
    }

    blackList(model: any) {
        return this.http.post('/api/'+this.name+'_black_list', model);
    }

    permission(model: any) {
        return this.http.post('/api/'+this.name+'_permission', model);
    }

    update(model: any) {
        return this.http.put('/api/'+this.name+'/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/'+this.name+'/' + id);
    }
}