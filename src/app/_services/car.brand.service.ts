import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class CarBrandService {
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/car_brands', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/car_brand/' + id);
    }

    create(model: any) {
        return this.http.post('/api/car_brand', model);
    }

    update(model: any) {
        return this.http.put('/api/car_brand/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/car_brand/' + id);
    }
}