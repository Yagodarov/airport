import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class TerminalService {

    constructor(private http: HttpClient) { }

    getParkings(params= {}) {
        return this.http.get<any[]>('/api/terminal/parkings', params);
    }

    order(model: any)
    {
        return this.http.post('/api/terminal/parking-order', model);
    }

    orderSearch(params= {}) {
        return this.http.get<any[]>('/api/terminal/cars/order_search', params);
    }
}