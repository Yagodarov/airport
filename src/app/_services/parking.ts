import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class Parking {
    name = 'parking';
    constructor(private http: HttpClient) { }

    getAll(params= {}) {
        return this.http.get<any[]>('/api/'+this.name+'s', params);
    }

    getById(id: number) {
        return this.http.get<any>('/api/'+this.name+'/' + id);
    }

    getInfo(id: number) {
        return this.http.get<any>('/api/'+this.name+'-info/' + id);
    }

    getCarParams()
    {
        return this.http.get<any>('/api/'+this.name+'_params');
    }

    order(model: any)
    {
        return this.http.post('/api/parking-order', model);
    }

    create(model: any) {
        return this.http.post('/api/'+this.name, model);
    }

    update(model: any) {
        return this.http.put('/api/'+this.name+'/' + model.id, model);
    }

    delete(id: number) {
        return this.http.delete('/api/'+this.name+'/' + id);
    }
}