import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {AlertService, AuthenticationService} from '../_services/index';
import {UserHelper} from '../_helpers/user.helper';

@Component({
  // moduleId: module.id.toString(),
  selector: 'login',
  templateUrl: 'login.component.html',
  providers: [UserHelper]
})

export class LoginComponent implements OnInit {
  model: FormGroup;
  loading = false;
  isHidden = false;
  returnUrl: string;
  error: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public user: UserHelper,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) {
    this.user.logout();
  }

  ngOnInit() {
    this.model = this.formBuilder.group({
      'username': '',
      'password': ''
    });
  }

  isLogged() {
    return this.user.getUserId();
  }

  login() {
    this.loading = true;
    this.error = '';
    // reset login status
    this.authenticationService.login(this.model.value.username, this.model.value.password)
      .subscribe(
        data => {
          this.isHidden = true;
          this.router.navigate([this.user.getReturnUrl()]);
        },
        error => {
          console.log(error);
          this.error = error['error'];
          this.loading = false;
        });
  }

  getSubmitButtonClass() {
    return {
      'icon-spinner2 position-right spinner': this.loading,
      'icon-circle-right2 position-right': !this.loading,
    };
  }
}
