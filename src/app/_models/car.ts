export class Car {
    brand_id;
    car_brand_id;
    number_of_passengers;
    baggage_count;
    report_documents;
    bank_card_payment: any = 0;
    external_baggage: any = 0;
    license: any = 0;
    baby_chair: any = 0;
    conditioner: any = 0;
    use_group_tariff: any = 0;
    image: any;
    id: any;
    getImagePath() {
        if (this.image) {
            return 'api/public/images/cars/' + this.id + '/' + this.image;
        } else {
            return 'api/public/images/cars/';
        }
    }
}