import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserService} from '../../_services';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
// const EXCEL_TYPE = 'application/octet-stream';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-car-ordered-by-user',
  templateUrl: './car-ordered-by-user.component.html',
  styleUrls: ['./car-ordered-by-user.component.css']
})

export class CarOrderedByUserComponent implements OnInit {
  pager;
  sort: any = 'id';
  desc = true;
  search: any = {
    user_id: '',
    action_id: '',
    limit : 10
  };
  columns = [
    {name: 'user_id', descr: 'Пользватель'},
    {name: 'car_id', descr: 'Автомобиль'},
    {name: 'cnt', descr: 'Кол-во заказов'}
  ];
  users_list = [];
  models: any;
  count: any;
  page: any = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private userService: UserService) {

  }

  ngOnInit() {
    this.getUsersList();
    this.loadModels();
  }

  loadModels() {
    return this.http.get<any[]>('/api/orders/ordered_by_user', {
      params: this.search
    }).subscribe(data => {
      console.log(data);
      this.models = data;
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getUsersList() {
    this.userService.getAll({params: {all: true, role: 'manager'}}).subscribe((data) => {
      this.users_list = data['models'];
    });
  }

  getUserFullName(item) {
    return item['surname'] + ' ' + item['first_name'];
  }

  private s2ab(s): any {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  public getExcelJsonForReport(json) {
    const resultArray = [];
    json.forEach((element) => {
      resultArray.push({
        'Пользователь': element['first_name'] + ' ' + element['surname'],
        'Автомобиль': element['name'] + ' ' + element['model'] + ' ' + element['number'],
        'Количество заказов': element['cnt']
      });
    });
    return resultArray;
  }

  public exportAsExcelFile(excelFileName: string): void {
    const json = this.getExcelJsonForReport(this.models);
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    const excelBuffer: any = XLSX.write(workbook, {bookType: 'xlsx', bookSST: true, type: 'binary'});
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([this.s2ab(buffer)], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }
}
