import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
// const EXCEL_TYPE = 'application/octet-stream';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-taxi-ordered',
  templateUrl: './taxi-ordered.component.html',
  styleUrls: ['./taxi-ordered.component.css']
})


export class TaxiOrderedComponent implements OnInit {
  pager;
  sort: any = 'id';
  desc: boolean = true;
  search: any = {
    limit: 10
  };
  models: any;
  columns = [
    {descr: 'Марка, модель'},
    {descr: 'Количество заказов'},
    {descr: ''},
  ];
  count: any;
  page: any = 1;

  constructor(private http: HttpClient) {
  }

  ngOnInit() {
    this.getMostOrdered();
  }

  private s2ab(s): any {
    const buf = new ArrayBuffer(s.length);
    const view = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  public getExcelJsonForReport(json) {
    const resultArray = [];
    json.forEach((element) => {
      resultArray.push({
        'Автомобиль': element['car']['car_brand']['name'] + ' ' + element['car']['model'] + ' ' + element['car']['number'],
        'Количество заказов': element['count']
      });
    });
    return resultArray;
  }

  public exportAsExcelFile(excelFileName: string): void {
    const json = this.getExcelJsonForReport(this.models);
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
    const excelBuffer: any = XLSX.write(workbook, {bookType: 'xlsx', bookSST: true, type: 'binary'});
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([this.s2ab(buffer)], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  getMostOrdered() {
    const most_ordered = this.http.get<any[]>('/api/orders/most_ordered', this.getParams()).subscribe(
      (data) => {
        this.models = data;
      }
    );
  }


  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort === data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.getMostOrdered();
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getParams() {
    const httpParams = new HttpParams();
    httpParams.append('page', String(this.page - 1));
    httpParams.append('orderBy', this.sort);
    httpParams.append('desc', String(this.desc));
    console.log(httpParams);
    const search = this.search;
    for (const key in search) {
      const value = search[key];
      console.log(key + ' ' + value);
      httpParams.append(key, value);
    }
    return {params: httpParams};
  }
}
