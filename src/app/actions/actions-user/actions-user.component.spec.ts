import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionsUserComponent } from './actions-user.component';

describe('ActionsUserComponent', () => {
  let component: ActionsUserComponent;
  let fixture: ComponentFixture<ActionsUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionsUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionsUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
