import {Component, OnInit, ElementRef, Renderer2} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ActionsUserService as Service, UserService} from '../../_services/index';

@Component({
  selector: 'app-actions-user',
  templateUrl: './actions-user.component.html',
  styleUrls: ['./actions-user.component.css']
})
export class ActionsUserComponent implements OnInit {
  pager;
  sort: any = 'id';
  desc: boolean = true;
  search = {
    user_id: '',
    action_id: ''
  };
  columns = [
    {name: 'id', descr: '#'},
    {name: 'created_at', descr: 'Дата'},
    {name: '', descr: 'Обьект события'},
    {name: 'user_id', descr: 'Пользователь'},
    {name: 'model_id', descr: 'Событие'}
  ];
  actions_list = [
    {
      id: 1,
      label: 'Добавлен пользователь'
    },
    {
      id: 2,
      label: 'Обновлен пользователь'
    },
    {
      id: 3,
      label: 'Пользователь заблокирован'
    },
    {
      id: 4,
      label: 'Пользователь разблокирован'
    },
    {
      id: 5,
      label: 'Авто добавлен'
    },
    {
      id: 6,
      label: 'Авто обновлен'
    },
    {
      id: 7,
      label: 'Водитель обновлен'
    },
    {
      id: 8,
      label: 'Водитель обновлен'
    },
    {
      id: 9,
      label: 'Авто занесен в черный список'
    },
    {
      id: 10,
      label: 'Авто удален из черного списка'
    },
    {
      id: 11,
      label: 'Вьезд(с оплатой)'
    },
    {
      id: 12,
      label: 'Вьезд'
    },
    {
      id: 13,
      label: 'Заказ'
    },
    {
      id: 14,
      label: 'Заказ(терминал)'
    },
    {
      id: 15,
      label: 'Выезд'
    },
  ];
  users_list = [];
  models: any;
  count: any;
  page: any = 1;

  constructor(
    private route: ActivatedRoute,
    private service: Service,
    private userService: UserService,
    private router: Router) {

  }

  ngOnInit() {
    this.getUsersList();
    this.route.data
      .subscribe(data => {
        console.log(data);
        this.models = data['data']['models'];
        this.count = data['data']['count'];
        this.page = 1;
      });
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort === data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getActionSubject(item) {
    if (item.model_id === 1) {
      if (item['user'] !== undefined) {
        const user = item['user'];
        return user['first_name'] + ' ' + user['surname'];
      } else {
        return '';
      }
    }
    if (item.model_id === 2) {
      if (item['car'] !== null) {
        const returnItem = item['car'];
        console.log(returnItem);
        let carBrand = '';
        if (returnItem && returnItem['car_brand']) {
          carBrand = returnItem['car_brand']['name'] + ' ';
        } else {
          carBrand = '';
        }
        return carBrand + returnItem['model'] + ' ' + returnItem['number'] + ' ' + returnItem['color'];
      } else {
        return '';
      }
    }
    if (item.model_id === 3) {
      if (item['driver'] !== null) {
        const driver = item['driver'];
        const driverReturn = driver['surname'] + ' ' + driver['first_name'] + ' ' + driver['patronymic'];
        let carReturn = '';
        if (item['car'] !== null) {
          const returnItem = item['car'];
          let carBrand = '';
          if (returnItem && returnItem['car_brand']) {
            carBrand = returnItem['car_brand']['name'] + ' ';
          } else {
            carBrand = '';
          }
          carReturn = carBrand + returnItem['model'] + ' ' + returnItem['number'] + ' ' + returnItem['color'] + '\n';
        }
        return carReturn + driverReturn;
      } else {
        return '';
      }
    }
  }

  subjectRedirect(item: any) {
    if (item.model_id === 1) {
      if (item['user'] !== undefined) {
        this.router.navigate(['/user/edit/' + item['user_id']]);
      }
      if (item.model_id === 2) {

      }
      if (item.model_id === 3) {

      }
    }
  }

  getUsersList() {
    this.userService.getAll({params: {all: true}}).subscribe((data) => {
      this.users_list = data['models'];
    });
  }

  getUserFullName(item) {
    return item['surname'] + ' ' + item['first_name'];
  }

  getByUserFullName(item) {
    return item['by_user']['surname'] + ' ' + item['by_user']['first_name'];
  }

  getTrueDate(date) {
    return moment.utc(date).local().format('DD.MM.YYYY HH:mm');
  }

  getEvent(item) {
    if (item['action_id'] !== null) {
      // this.actions_list.find(x => x.id === item['action_id']).label;
      return this.actions_list.find(x => x.id === item['action_id']).label;
    } else {
      return '';
    }
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    console.log(this.search);
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }
}
