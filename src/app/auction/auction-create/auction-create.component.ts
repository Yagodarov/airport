import {Component, OnInit} from '@angular/core';
import {AuctionService as Service} from '../../_services';
import {Router} from '@angular/router';
declare var $ : any;
declare var google : any;
@Component({
  selector: 'app-auction-create',
  templateUrl: './auction-create.component.html',
  styleUrls: ['./auction-create.component.css']
})
export class AuctionCreateComponent implements OnInit {
  src = '';
  googleInput: HTMLInputElement;
  model: any = {
    address : '',
    number_of_passengers : '',
    client_phone_number : '',
    report_documents : 0,
    bank_card_payment : 0,
    external_baggage : 0,
    // license : 0,
    baby_chair : 0,
    status : 1,
    // conditioner : 0,
  };
  errors: any = [];

  constructor(private service: Service, private router: Router) {
  }

  submitted = false;

  onSubmit() {
    $('#create_button').attr("disabled", "disabled");
    this.service.create(this.model).subscribe(data => {
        this.service.chooseWinner(data).subscribe((result) => {
          console.log(result);
        }, (result) => {
          console.log(result);
        });
        this.router.navigate(['mobile/auctions']);
      },
      error => {
        $('#create_button').removeAttr("disabled");
        console.log(error['error']);
        this.errors = error['error'];
      });
    console.log(this.model);
  }

  initGoogleSearch() {
    this.googleInput = /** @type {!HTMLInputElement} */(
      $('#address').get(0));
    const autocomplete = new google.maps.places.Autocomplete(this.googleInput);

    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const val = $('#address').val();
      this.model['address'] = val;
      // this.calculateAndDisplayRoute();
    });
  }

  ngOnInit() {
    this.initGoogleSearch();
  }

  hasError(field) {
    let classList = {
      'has-error': this.errors[field]
    };
    return classList;
  }
}
