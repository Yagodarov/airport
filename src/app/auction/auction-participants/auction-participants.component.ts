import {Component, OnInit} from '@angular/core';
import {Brand} from '../../_models/brand';
import {ActivatedRoute} from '@angular/router';
import {BrandService as Service} from '../../_services';

@Component({
  selector: 'app-auction-participants',
  templateUrl: './auction-participants.component.html',
  styleUrls: ['./auction-participants.component.css']
})
export class AuctionParticipantsComponent implements OnInit {
  pager;
  sort: string = 'id';
  desc: boolean = true;
  search = [];
  columns = [
    {name: 'id', descr: '#'},
    {name: 'driver_id', descr: 'Водитель'},
    {name: 'km_price', descr: 'Цена за км'},
    {name: 'total_price', descr: 'Итоговая цена'}
  ];
  model = new Brand();
  models: any;
  count: any;
  page: any = 1;

  constructor(private route: ActivatedRoute, private service: Service) {

  }

  ngOnInit() {
    this.route.data
      .subscribe(data => {
        // console.log(data);
        this.models = data['data']['participants'];
        // this.count = data['data']['count'];
        // this.page = 1;
      });
  }

  delete(id: number) {
    if (confirm('Точно удалить?'))
      this.service.delete(id).subscribe(() => {
        this.loadModels();
      });
  }

  setPage(page) {
    this.page = page;
    this.loadModels();
  }

  setSort(data) {
    if (this.sort == data)
      this.desc = !this.desc;
    else
      this.desc = true;
    this.sort = data;
    this.loadModels();
  }

  loadModels() {
    this.service.getAll(this.getParams()).subscribe(data => {
      this.models = data['models'];
      this.count = data['count'];
    });
  }

  inputSearch() {
    this.page = 1;
    this.loadModels();
  }

  getFullName(item) {
    return item['surname'] + ' ' + item['first_name'];
  }

  getParams() {
    var params = {
      page: String(this.page - 1),
      orderBy: this.sort,
      desc: this.desc,
    };
    var search = this.search;
    for (let key in search) {
      let value = search[key];
      console.log(key + ' ' + value);
      params[key] = value;
    }
    return {params: params};
  }
}
