/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {DriverCarService as Service} from "../_services/driver-car";

@Injectable()
export class CarDriversResolver implements Resolve<any[]>{
  constructor(private service: Service) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any[]> {
    return this.service.getAll({ params: {car_id : Number(route['_urlSegment']['segments'][1]['path'])}});
  }

}