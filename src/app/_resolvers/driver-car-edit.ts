/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {DriverCarService as Service}  from "../_services/index";

@Injectable()
export class DriverCarEditResolver implements Resolve<any[]>{
    constructor(private service: Service) {

    }
    resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any[]> {
        console.log(route.params);
        return this.service.getById(Number(route.params.id));
    }

}