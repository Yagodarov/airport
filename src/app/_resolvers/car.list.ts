/**
 * Created by Алексей on 09.02.2018.
 */
import {Resolve} from "@angular/router";
import {Observable} from "rxjs";
import {RouterStateSnapshot} from "@angular/router";
import {ActivatedRouteSnapshot} from "@angular/router";
import {Injectable} from "@angular/core";
import {CarService as Service}  from "../_services/index";

@Injectable()
export class CarsListResolver implements Resolve<any[]>{
  constructor(private service: Service) {

  }
  resolve(route:ActivatedRouteSnapshot, state:RouterStateSnapshot):Observable<any[]> {
    return this.service.getAll({params:{all:true}});
  }

}