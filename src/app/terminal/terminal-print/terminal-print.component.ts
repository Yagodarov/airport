import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Parking } from '../../_services/index';
import { Tariff } from '../../_models/tariff';
declare var moment : any;
declare var window : any;
@Component({
  selector: 'app-terminal-print',
  templateUrl: './terminal-print.component.html',
  styleUrls: ['./terminal-print.component.css']
})

export class TerminalPrintComponent implements OnInit {
	printModel : any;
	id : any;
	tariffs : any;
	tariffsKeys : any;
  constructor(private service: Parking, private route: ActivatedRoute) {
  	window.bounce = function(w) {

	        window.blur();
	        console.log(w.id);
	        w.focus();
	}
  	route.params.subscribe((data) => {
  		this.id = data['id']
  	});
  	service.getInfo(this.id).subscribe((data) => {
  		this.tariffs = Object.assign(new Tariff, data);
  		this.tariffsKeys = Object.keys(this.tariffs);
  		this.printModel = data;
  	});
  	;
  }

  dateFormat(format)
    {
      return moment().format(format);
    }

  ngOnInit() {
  }

}
