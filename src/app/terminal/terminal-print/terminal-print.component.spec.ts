import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalPrintComponent } from './terminal-print.component';

describe('TerminalPrintComponent', () => {
  let component: TerminalPrintComponent;
  let fixture: ComponentFixture<TerminalPrintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminalPrintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
