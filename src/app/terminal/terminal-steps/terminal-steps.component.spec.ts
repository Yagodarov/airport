import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerminalStepsComponent } from './terminal-steps.component';

describe('TerminalStepsComponent', () => {
  let component: TerminalStepsComponent;
  let fixture: ComponentFixture<TerminalStepsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerminalStepsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerminalStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
