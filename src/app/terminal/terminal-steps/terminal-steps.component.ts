import {Component, OnInit, ChangeDetectorRef} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {TerminalService, PrintService} from '../../_services/index';
import {ParkingModel, Car, CarImage, Driver} from '../../_models/index';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Idle} from 'idlejs/dist';

declare let $: any;
declare let google: any;

declare let moment: any;

@Component({
    selector: 'app-terminal-steps',
    templateUrl: './terminal-steps.component.html',
    styleUrls: ['./terminal-steps.component.css']
})
export class TerminalStepsComponent implements OnInit {
    public directionsService;
    public directionsDisplay;
    public marker;
    public cars: ParkingModel[] = [];
    public length;
    orderedModel: ParkingModel;
    image_url = '';
    length_text = '';
    timeout = 90;
    car_not_found: boolean;
    time: any;
    googleInput: HTMLInputElement;
    map: any;
    directionResult: any;
    parking: any;
    errors = [];
    model: FormGroup;
    totalCount: Number = 0;

    constructor(
        public formBuilder: FormBuilder,
        public route: ActivatedRoute,
        public http: HttpClient,
        public router: Router,
        public cd: ChangeDetectorRef,
        public terminal: TerminalService
    ) {
        this.initImage();
    }

    initImage() {
        function randomInteger(min, max) {
            let rand = min + Math.random() * (max + 1 - min);
            rand = Math.floor(rand);
            return rand;
        }

        this.image_url = '/assets/images_random/' + randomInteger(1, 4) + '.jpg';
    }

    ngOnInit() {
        this.initModel();
        this.initSteps();
        this.initGoogleMap();
        this.initGoogleSearch();
        this.initRouteDate();
        this.hideCopyright();
        this.initIdleAction();
        this.initFocusInput();
        window['angular'] = this;
    }

    initFocusInput() {
        setTimeout(function () {
            $('#address_to').focus();
        }, 100);
    }

    reloadPage() {
        window.location.reload();
    }

    dateFormat(format) {
        return moment().format(format);
    }

    setOrdered(model) {
        this.orderedModel = model;
    }

    openModal(model, event) {
        event.target.className = 'btn btn-success';
        event.target.innerText = 'Заказать';
        this.parking = null;
        this.parking = model;
        this.parking['address'] = this.model.controls['address'].value;
        this.parking['length'] = this.model.controls['length'].value;
        this.time = new Date;
    }

    onSubmit() {

    }

    initIdleAction() {
        const idle = new Idle()
            .whenNotInteractive()
            .within(this.timeout, 1000)
            .do(() => this.reloadPage())
            .start();
    }

    initRouteDate() {
        this.route.data
            .subscribe(data => {
                this.totalCount = data['cars']['count'];
                this.timeout = data['options']['terminal_timeout'] ? data['options']['terminal_timeout'] : 90;
            }, error => {

            });
    }

    initModel() {
        this.model = this.formBuilder.group({
            address: new FormControl(''),
            length: new FormControl(0),
            report_documents: new FormControl(false),
            baby_chair: new FormControl(false),
            external_baggage: new FormControl(false),
            bank_card_payment: new FormControl(false)
        });
    }

    echoModel() {
        console.log(this.model.value);
    }

    initGoogleMap() {
        const start = {lat: 54.569444, lng: 55.9016671};
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;

        this.map = new google.maps.Map(document.getElementById('map'), {
            center: start,
            zoom: 13,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.HYBRID,
            // gestureHandling: 'greedy'
        });

        this.directionsDisplay.setMap(this.map);

        this.marker = new google.maps.Marker({
            position: start,
            map: this.map,
        });
        this.marker.setMap(this.map);

        $('#show_map_route').click(() => {
            this.calculateAndDisplayRoute((result) => {
                this.cd.detectChanges();
            });
        });
    }

    hideCopyright() {
        setInterval(
            () => {
                $('#map > div > div > div:nth-child(3)').html('');
                $('#map > div > div > div.gmnoprint.gm-style-cc').html('');
                $('#map > div > div > div:nth-child(9)').html('');
            }, 1000);

    }

    initGoogleSearch() {
        this.googleInput = /** @type {!HTMLInputElement} */(
            $('#address_to').get(0));
        const autocomplete = new google.maps.places.Autocomplete(this.googleInput);
        autocomplete.bindTo('bounds', this.map);

        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const val = $('#address_to').val();
            this.model.controls['address'].setValue(val);
            // this.calculateAndDisplayRoute();
        });
    }

    setGoogleMapOptions(callback = null) {
        setTimeout(google.maps.event.trigger(this.map, 'resize'), 250);
        callback ? callback() : null;
    }

    getCarName() {
        const model = this.orderedModel;
        return model.car['car_brand'].name + ' ' + model.car['model'] + ' ' + model.car['number'] + ' ' + model.car['color'];
    }

    goToStep(number) {
        if (number === 2) {
            this.calculateRoute((result) => {
                this.removeErrors('address');
                if (result) {
                    this.getCarList((data, success) => {
                        this.car_not_found = false;
                        if (success) {
                            this.cars = [];
                            data['models'].forEach((item) => {
                                const filledparking = Object.assign(new ParkingModel, item);
                                filledparking.car = Object.assign(new Car, item['car']);
                                filledparking.driver = Object.assign(new Driver, item['driver']);
                                filledparking.car_images = [];
                                if (item['car']['images']) {
                                    item['car']['images'].forEach((item) => {
                                        filledparking['car_images'].push(Object.assign(new CarImage, item));
                                    });
                                }
                                this.cars.push(filledparking);
                            });
                            this.cars.sort((a, b) => {
                                return a.price - b.price;
                            });
                            this.calculateRoute();
                            $('#terminal-steps').steps('next');
                        } else {
                            this.car_not_found = true;
                            $('#terminal-steps').steps('next');
                        }
                    });
                } else {
                    this.setError('address', 'Уточните адрес или выберите адрес из списка');
                }
            });
        }
        if (number === 3) {
            $('#terminal-steps').steps('next');
        }
    }

    goToPreviousStep() {
        $('#terminal-steps').steps('previous');
    }

    getCarList(callback) {
        this.terminal.orderSearch({params: this.model.value}).subscribe((data) => {
            callback(data, true);
        }, (error) => {
            callback(error, false);
        });
    }

    calculateRoute(callback = null) {
        const waypts = [];
        this.directionsService.route({
            origin: 'Аэропорт, Республика Башкортостан, Россия',
            destination: $('#address_to').val(),
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, (response, status) => {
            this.setLengthByResponse(response);
            if (status === 'OK') {
                callback ? callback(true) : null;
            } else {
                this.setError('address', 'Не удалось проложить маршрут');
                callback ? callback(false) : null;
            }
        });
    }

    calculateAndDisplayRoute(callback = null) {
        console.log('calculateanddisplayroute');
        const waypts = [];
        this.directionsService.route({
            origin: 'Аэропорт, Республика Башкортостан, Россия',
            destination: $('#address_to').val(),
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: 'DRIVING'
        }, (response, status) => {
            if (status === 'OK') {
                this.setLengthByResponse(response);
                this.directionsDisplay.setDirections(response);
                this.removeErrors('address');
                this.directionResult = response.routes[0].bounds;
                callback ? callback(true) : null;
            } else {
                this.setError('address', 'Не удалось проложить маршрут');
                callback ? callback(false) : null;
            }
        });
    }

    setLengthByResponse(response) {
        this.model.controls['length'].setValue(response.routes['0'].legs['0'].distance.value);
        this.length_text = response.routes['0'].legs['0'].distance.text;
    }

    removeErrors(property) {
        this.errors[property] = undefined;
        this.cd.detectChanges();
    }

    setError(property, value) {
        this.errors[property] = [value];
        this.cd.detectChanges();
    }

    initSteps() {
        setTimeout(
            () => {
                $('#terminal-steps').steps({
                    headerTag: 'h3',
                    bodyTag: 'section',
                    transitionEffect: 'fade',
                    enablePagination: false,
                    onStepChanging: (event, currentIndex, newIndex) => {
                        return true;
                    },
                    onInit: (event, currentIndex) => {
                    },
                    onStepChanged: (event, currentIndex, prevIndex) => {
                        if (prevIndex > currentIndex) {
                            return true;
                        }
                        if (currentIndex === 1) {
                            // this.initGoogleMap();
                            this.calculateAndDisplayRoute((result) => {
                                google.maps.event.trigger(this.map, 'resize');
                            });
                        }
                    },
                    onFinished: (event, currentIndex) => {
                        this.onSubmit();
                    },
                    labels: {
                        cancel: 'Отмена',
                        current: 'current step:',
                        pagination: 'Pagination',
                        finish: 'Подтвердить',
                        next: 'Вперед',
                        previous: 'Назад',
                        loading: 'Loading ...'
                    }
                });
            }, 100);
    }

    hasError(field) {
        const classList = {
            'has-error': this.errors[field] !== undefined
        };
        return classList;
    }

}
