import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Event, NavigationEnd,} from '@angular/router';
import {UserHelper} from './_helpers/user.helper';
import {Location} from '@angular/common';

@Component({
  // moduleId: module.id.toString(),
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserHelper]
})
export class AppComponent implements OnInit {
  logged: boolean;

  constructor(
    private user: UserHelper,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) {
    // console.log(route);
    this.logged = user.getUserId() ? true : false;
  }

  ngOnInit() {
      this.router.events.subscribe((event: any) => {
        // console.log(event);
        // if (event.error) {
        //   this.router.navigate(['/login']);
        // }
        if (event.url && event.url === '/') {
          if (this.user.getUserRole()) {
            this.router.navigate([this.user.getStartUrl()]);
          }
          else
            this.router.navigate(['/login']);
        }
        if (event.urlAfterRedirects && event.urlAfterRedirects == '/') {

          this.router.navigate(['/not-found']);
        }
        if (event instanceof NavigationEnd) {
          // console.log(event);
          // console.log(this);
        }
      });
  }
}
