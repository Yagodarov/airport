<?php

return [

    'appNameIOS'     => [
        'environment' =>'development',
        'certificate' =>'/path/to/certificate.pem',
        'passPhrase'  =>'password',
        'service'     =>'apns'
    ],
    'appNameAndroid' => [
        'environment' =>'production',
        'apiKey'      =>'AAAA-zDkwV0:APA91bHbeJvKhnkj2TY6P6ldfxsi4Ze3qNu3GWkI6fqoz0kLlAzss69mgQq7qBPrKzCYrvnPjQmPUrvdAN8i5Apu4a3Ev5IzFihcAWsjYYHShK6XXSAmXiEWRlQmcwPZY03qr7Gno26N',
        'service'     =>'gcm'
    ]

];
