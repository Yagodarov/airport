<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTariffFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->integer('type_tariff_use')->default(1);
            $table->integer('starting_rate')->nullable();
            $table->integer('town_km_price')->nullable();
            $table->integer('countryside_km_price')->nullable();
            $table->string('comment_km_price')->nullable();
        });

        Schema::table('driver_tariffs', function (Blueprint $table) {
            $table->integer('type_tariff_use')->default(1);
            $table->integer('starting_rate')->nullable();
            $table->integer('town_km_price')->nullable();
            $table->integer('countryside_km_price')->nullable();
            $table->string('comment_km_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tariffs', function (Blueprint $table) {
            $table->dropColumn('type_tariff_use');
            $table->dropColumn('starting_rate');
            $table->dropColumn('town_km_price');
            $table->dropColumn('countryside_km_price');
            $table->dropColumn('comment_km_price');
        });
        Schema::table('driver_tariffs', function (Blueprint $table) {
            $table->dropColumn('type_tariff_use');
            $table->dropColumn('starting_rate');
            $table->dropColumn('town_km_price');
            $table->dropColumn('countryside_km_price');
            $table->dropColumn('comment_km_price');
        });
    }
}
