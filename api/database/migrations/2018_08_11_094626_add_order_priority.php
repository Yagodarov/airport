<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderPriority extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('drivers', function (Blueprint $table) {
        $table->integer('terminal_countryside_order_priority')->default(1);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('drivers', function (Blueprint $table) {
        $table->dropColumn('terminal_countryside_order_priority')->default(1);
      });
    }
}
