<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTariffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name")->default('');
            $table->integer("town_center")->nullable();
            $table->integer("town")->nullable();
            $table->integer("periphery")->nullable();
            $table->integer("km_price")->nullable();
            $table->string("custom_tariff")->nullable();
            $table->integer("use_custom_tariff")->default(0);
            $table->text("comment")->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tariffs');
    }
}
