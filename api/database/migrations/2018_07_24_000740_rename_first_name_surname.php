<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameFirstNameSurname extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
           $table->renameColumn('first_name', 'temp_name');
        });
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('surname', 'first_name');
        });
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('temp_name', 'surname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('surname', 'temp_name');
        });
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('first_name', 'surname');
        });
        Schema::table('users', function(Blueprint $table){
            $table->renameColumn('temp_name', 'first_name');
        });
    }
}
