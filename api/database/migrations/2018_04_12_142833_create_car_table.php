<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_brand_id')->unsigned();
            $table->integer('class_id')->unsigned();
            $table->integer('group_tariff_id')->unsigned();
            $table->integer('client_type_id')->unsigned();
            $table->integer('brand_id')->unsigned();
            $table->string('model')->nullable();
            $table->integer('year')->nullable();
            $table->string('number')->nullable();
            $table->string('color')->nullable();
            $table->integer('number_of_passengers')->nullable();
            $table->integer('baby_chair')->nullable();
            $table->integer('conditioner')->nullable();
            $table->integer('baggage_count')->nullable();
            $table->integer('permission_to_drive')->nullable();
            $table->integer('is_black_list')->nullable();
            $table->integer('external_baggage')->nullable();
            $table->integer('license')->nullable();
            $table->text('license_requisites')->nullable();
            $table->integer('use_group_tariff')->nullable();
            $table->integer('bank_card_payment')->nullable();
            $table->integer('report_documents')->nullable();

            $table->foreign('car_brand_id')
                ->references('id')
                ->on('car_brands')
                ->onDelete('cascade');

            $table->foreign('class_id')
                ->references('id')
                ->on('car_classes')
                ->onDelete('cascade');

            $table->foreign('brand_id')
                ->references('id')
                ->on('brands')
                ->onDelete('cascade');

            $table->foreign('group_tariff_id')
                ->references('id')
                ->on('tariffs')
                ->onDelete('cascade');

            $table->foreign('client_type_id')
                ->references('id')
                ->on('client_types')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
