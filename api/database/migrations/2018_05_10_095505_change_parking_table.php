<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeParkingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking', function (Blueprint $table) {
            $table->string('town')->nullable()->change();
            $table->string('town_center')->nullable()->change();
            $table->string('km_price')->nullable()->change();
            $table->string('periphery')->nullable()->change();
            $table->integer('use_new_tariffs')->nullable()->change();
            $table->integer('use_custom_tariff')->nullable()->change();
            $table->string('custom_tariff')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropTable('parking');
    }
}
