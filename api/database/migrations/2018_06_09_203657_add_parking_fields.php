<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParkingFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking', function (Blueprint $table) {
            $table->string('type_tariff_use')->nullable();
            $table->string('starting_rate')->nullable();
            $table->string('town_km_price')->nullable();
            $table->string('countryside_km_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking', function (Blueprint $table) {
            $table->dropColumn('type_tariff_use');
            $table->dropColumn('starting_rate');
            $table->dropColumn('town_km_price');
            $table->dropColumn('countryside_km_price');
        });
    }
}
