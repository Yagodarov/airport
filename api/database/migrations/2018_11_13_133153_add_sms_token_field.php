<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSmsTokenField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
          $table->string('sms_token');
        });
        Schema::table('users', function (Blueprint $table) {
          $table->string('push_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('drivers', function (Blueprint $table) {
        $table->dropColumn('sms_token');
      });
      Schema::table('users', function (Blueprint $table) {
        $table->dropColumn('push_token');
      });
    }
}
