<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParkingFrom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('parking', function (Blueprint $table) {
            $table->string('order_from')->default('');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->string('order_from')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parking', function (Blueprint $table) {
            $table->dropColumn('order_from');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('order_from');
        });
    }
}