<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActionsActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('user_actions_actions', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
        });

      Schema::table('user_actions', function (Blueprint $table) {
        $table->dropForeign('user_actions_action_id_foreign');
        $table->foreign('action_id')
          ->references('id')
          ->on('user_actions_actions')
          ->onDelete('cascade')
          ->change();
      });
      DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
      Schema::table('user_actions', function (Blueprint $table) {
        $table->dropForeign('user_actions_action_id_foreign');
        $table->foreign('action_id')
          ->references('id')
          ->on('actions')
          ->onDelete('cascade')
          ->change();
      });
        Schema::dropIfExists('user_actions_actions');
      DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
