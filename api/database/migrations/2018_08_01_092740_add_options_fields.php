<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOptionsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('options', function (Blueprint $table) {
            $table->string('phone_feedback')->nullable()->default('89871458288');
            $table->string('email_feedback')->nullable()->default('airavalon2007@yandex.ru');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('options', function (Blueprint $table) {
            $table->dropColumn('phone_feedback');
            $table->dropColumn('email_feedback');
        });
    }
}
