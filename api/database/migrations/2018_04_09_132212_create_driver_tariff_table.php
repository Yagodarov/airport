<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriverTariffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_tariffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("town_center")->nullable();
            $table->integer("town")->nullable();
            $table->integer("periphery")->nullable();
            $table->integer("km_price")->nullable();
            $table->string("custom_tariff")->nullable();
            $table->integer("use_custom_tariff")->default(0);
            $table->foreign('id')
                ->references('id')
                ->on('drivers')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_tariffs');
    }
}
