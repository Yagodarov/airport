<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusFieldToPhoneRegisterRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('phone_register_requests', function(Blueprint $table) {
          $table->integer('status')->default(0);
          $table->integer('driver_id')->unsigned()->nullable();
          $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
        });
      DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS=0');
      Schema::table('phone_register_requests', function(Blueprint $table) {
        $table->dropColumn('status');
        $table->dropForeign(['driver_id']);
        $table->dropColumn('driver_id');
      });
      DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
