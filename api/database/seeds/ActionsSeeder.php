<?php

use App\Http\Models\ActionUser;
use Illuminate\Database\Seeder;
use App\Http\Models\Action;
use Illuminate\Support\Facades\DB;

class ActionsSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::statement('SET FOREIGN_KEY_CHECKS=0');
    DB::table('actions')->truncate();
//    $actions = [
//      'Новое авто',
//      'Новый водитель',
//      'Новый в черном списке',
//      'Въезд',
//      'Заказ',
//      'Выезд',
//      'Заказ(терминал)',
//      'Вьезд(с оплатой)',
//    ];
//    $user_actions = [
//      'Новое авто',
//      'Новый водитель',
//      'Новый в черном списке',
//      'Въезд',
//      'Заказ',
//      'Выезд',
//      'Заказ(терминал)',
//      'Вьезд(с оплатой)',
//      'Выход',
//      'Вход',
//    ];
//    foreach ($actions as $key => $value) {
//      $model = new Action();
//      $model->name = $value;
//      $model->save();
//    }
//    foreach ($user_actions as $key => $value) {
//      $model = new ActionUser();
//      $model->name = $value;
//      $model->save();
//    }
    DB::statement('SET FOREIGN_KEY_CHECKS=1');
  }
}
