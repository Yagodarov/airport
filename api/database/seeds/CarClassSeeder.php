<?php

use Illuminate\Database\Seeder;
use App\Http\Models\CarClass;
class CarClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $classes = [
        	'A', 'B', 'C', 'D', 'E', 'F', 'MPV', 'SUV', 'CUV', 'mBUS'
        ];
        foreach($classes as $key => $value)
        {
        	$model = new CarClass();
        	$model->id = $key + 1;
        	$model->name = $value;
        	$model->save();
        }
    }
}
