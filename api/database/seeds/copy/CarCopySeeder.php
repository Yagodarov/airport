<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class CarCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('car')->select('*')->get();
        foreach ($rows as $row)
        {
            $car = new \App\Http\Models\Car();
            $car->fill([
                'id' => $row->id,
                'car_brand_id' => $row->company,
                'class_id' => $row->class,
                'group_tariff_id' => $row->group_tariff_id,
                'client_type_id' => $row->client_type_id,
                'brand_id' => $row->brand_id,
                'model' => $row->model,
                'year' => $row->year,
                'number' => $row->number,
                'color' => $row->color,
                'number_of_passengers' => $row->number_of_passengers,
                'baby_chair' => $row->baby_chair,
                'image' => $row->image,
                'permission_to_drive' => $row->permission_to_drive,
                'conditioner' => $row->conditioner,
                'baggage_count' => $row->baggage_count,
                'is_black_list' => $row->is_black_list,
                'external_baggage' => $row->external_baggage,
                'license' => $row->license,
                'license_requisites' => $row->license_requisites,
                'use_group_tariff' => $row->use_group_tariff,
                'bank_card_payment' => $row->bank_card_payment,
                'report_documents' => $row->report_documents,
            ]);
            if ($row->image)
            {
                $car->saveImageFromUrl("http://airportufataxi.ru/web/files/images/".$row->image);
                if ($car->save()) {
                    continue;
                }
                else {
                    echo "error";
                    break;
                }
            }
        }
    }
}