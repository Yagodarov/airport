<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class BrandCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('brand')->select('*')->get();
        foreach ($rows as $row)
        {
            $brand = new \App\Http\Models\Brand();
            $brand->name = $row->name;
            $brand->image = $row->image;
            $brand->id = $row->id;
            if ($result = $brand->saveImage("http://airportufataxi.ru/web/files/images/brand/".$row->image) && $result = $brand->save())
            {
                continue;
            }
            else
            {
                echo $result;
                break;
            }
        }
    }
}