<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class DriverCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('driver')->select('*')->get();
        foreach ($rows as $row)
        {
            $driver = new \App\Http\Models\Driver();
            $driver->fill([
                'id' => $row->id,
                'first_name' => $row->name,
                'surname' => $row->surname,
                'patronymic' => $row->middle_name,
                'image' => $row->photo_image,
                'serial_number' => $row->serial_number,
                'experience' => $row->experience,
                'phone_number' => $row->phone_number,
                'phone_number_2' => $row->phone_number_2,
                'email' => $row->email,
                'comment' => $row->comment,
                'raiting' => $row->rating,
                'license' => $row->license,
                'passport_number' => $row->passport_number,
                'driver_card_serial_number' => $row->driver_card_serial_number,
                'reviews_text' => $row->reviews_text,
                'mvd_review_data' => $row->mvd_review_data,
            ]);
            if ($row->photo_image)
            {
                $driver->saveImageFromUrl("http://airportufataxi.ru/web/files/images/drivers/".$row->photo_image);
            }
            if ($driver->save()) {
                continue;
            }
            else {
                echo "error";
                break;
            }
        }
    }
}