<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.06.2018
 * Time: 12:31
 */
class CarBrandCopySeeder extends Seeder
{
    public function run()
    {
        $rows = \Illuminate\Support\Facades\DB::connection('old')->table('car_brand')->select('*')->get();
        foreach ($rows as $row)
        {
            $carBrand = new \App\Http\Models\CarBrand();
            $carBrand->id = $row->id;
            $carBrand->name = $row->name;
            $carBrand->save();
        }
    }
}