<?php

/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.12.2017
 * Time: 22:10
 */
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ZonesSeeder extends Seeder
{
    public function run(){
        app()['cache']->forget('spatie.permission.cache');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        DB::table('areas')->truncate();

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}