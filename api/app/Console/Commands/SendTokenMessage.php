<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Models\PushNotification;
class SendTokenMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      (new PushNotification())
        ->push(
          "c39CK48fyNE:APA91bEwDNa1L7cPExRH6p9opn5W4wv05cORCVinQzRGtU1gSliF",
          'Пришло?',
          'Пришло?',
          [
            'id' => 123,
            'address' => 123,
            'comment' => 123
            ]);
    }
}
