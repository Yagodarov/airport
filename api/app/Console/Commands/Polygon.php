<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Davibennun\LaravelPushNotification\Facades\PushNotification;
use App\Http\Models\Gmaps\PointLocation;
class Polygon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:polygon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $polygons = DB::table('areas')->where('zone', '=', 'town')->get();
        $point = (object) [
            'lat' =>  -33,
            'lng' => 151,
        ];
        var_dump($polygons);
        $pointLocation = (new PointLocation())->pointInPolygon($point, $polygons);
        echo $pointLocation;
    }
}
