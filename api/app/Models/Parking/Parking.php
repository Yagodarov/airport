<?php

namespace App\Http\Models\Parking;

use App\Http\Models\Action;
use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Models\DriverCar;
use App\Http\Models\DriverTariff;
use App\Http\Models\Report;
use App\Http\Models\Driver;
use App\Http\Models\Order;
use App\Http\Models\Car;
use App\Http\Models\Tariff;
use App\Http\Models\UserAction;
class Parking extends Model
{
    protected static function boot()
    {
        parent::boot();

        static::created(function (Parking $model) {
          $report = new Action();
          $report->action_id = 12;
          $report->model_id = 3;
          $report->car_id = $model->car_id;
          $report->driver_id = $model->driver_id;
          $report->by_user_id = \Auth::user()->id;
          $report->save();
        });

        static::updated(function (Parking $model) {

        });

        static::deleting(function (Parking $model) {
            if ($model->order_from == 'parking')
            {
                $report = new Action();
                $report->action_id = 13;
                $report->model_id = 3;
                $report->car_id = $model->car_id;
                $report->driver_id = $model->driver_id;
                $report->by_user_id = \Auth::user()->id;
                $report->save();
            }
            elseif ($model->order_from == 'terminal')
            {
                $report = new Action();
                $report->action_id = 14;
                $report->model_id = 3;
                $report->car_id = $model->car_id;
                $report->driver_id = $model->driver_id;
                $report->by_user_id = \Auth::user()->id;
                $report->save();
            }
            else
            {
                $report = new Action();
                $report->action_id = 15;
                $report->model_id = 3;
                $report->car_id = $model->car_id;
                $report->driver_id = $model->driver_id;
                $report->by_user_id = \Auth::user()->id;
                $report->save();
            }
        });
    }
    protected $table="parking";
    public $zone;
    protected $fillable = [
        'driver_id',
        'car_id',
        'town',
        'town_center',
        'km_price',
        'periphery',
        'use_new_tariffs',
        'use_custom_tariff',
        'custom_tariff',
        'type_tariff_use',
        'starting_rate',
        'town_km_price',
        'order_from',
        'countryside_km_price',
    ];

    public function rules()
    {
        return [
            'driver_id' => 'required|unique:parking,driver_id',
            'car_id' => ['required','unique:parking,car_id', function($attribute, $value, $fail){
                $car = Car::find($value);
                if ($car->is_black_list)
                {
                    $fail("Автомобиль в черном списке");
                }
                if (!$car->permission_to_drive)
                {
                    $fail("Автомобиль деактивирован");
                }
            }],
            'town',
            'town_center',
            'km_price',
            'periphery',
            'use_new_tariffs',
            'use_custom_tariff',
            'custom_tariff',
            'type_tariff_use',
            'starting_rate',
            'town_km_price',
            'countryside_km_price',
        ];
    }

    public static function search(Request $request)
    {
        $models =  Parking::
            with('car')
            ->with('car.carBrand')
            ->with('car.brand')
            ->with(['driver' => function($models){
                $models->orderBy('drivers.raiting', 'desc');
            }])
            ->with('brand')
            ->when($request->get('orderBy'), function ($models) use ($request) {
                return $models
                    ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
            })
            ->orderBy('parking.town_center', 'asc')
            ->groupBy('id');

        $count = $models->get()->count();
        $models = $models
            ->get();
        return response()->json([
            'models' => $models,
            'count' => $count
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }
        else
        {
            $this->fill($request->all());
            $this->checkIsGroupTariffThenFill();
            if (!$this->driver->entry_to_parking)
            {
                return response()->json(true, 200);
            }
            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
                return response()->json($result, 403);
        }
    }

    public function checkIsGroupTariffThenFill()
    {
        $car = Car::where('id', $this->car_id)->first();
        /* Если автомобиль с групповыми тарифами и эти тарифы есть */
        if ($car->use_group_tariff && $car->group_tariff_id)
        {
            $groupTariff = Tariff::where('id', $car->group_tariff_id)->first();
            /* Если единый тариф */
            // abort(403, $groupTariff->id);
            if ($groupTariff->use_custom_tariff && $groupTariff->type_tariff_use == 1)
            {
                $this->fillCustomTariff($this);
            }
            /* Если тарифы старого типа*/
            elseif ($groupTariff->type_tariff_use == 1)
            {
                $this->fillTariffs($groupTariff);
            }
            /* Если тариф нового типа*/
            elseif ($groupTariff->type_tariff_use == 2)
            {
                $this->fillNewTariffs($groupTariff);
            }
        }
        /* Если используем новые тарифы */
        elseif ($this->use_new_tariffs)
        {
            if ($this->use_custom_tariff)
            {
                $this->fillCustomTariff($this);
            }

        }
        else
        {
            $driverTariff = DriverTariff::where('id', $this->driver_id)->first();
            if ($driverTariff->use_custom_tariff && $driverTariff->type_tariff_use == 1)
            {
                $this->fillCustomTariff($driverTariff);
            }
            /* Если тарифы старого типа*/
            elseif ($driverTariff->type_tariff_use == 1)
            {
                $this->fillTariffs($driverTariff);
            }
            /* Если тариф нового типа*/
            elseif ($driverTariff->type_tariff_use == 2)
            {
                $this->fillNewTariffs($driverTariff);
            }
        }
    }

    public function fillNewTariffs($tariffs)
    {
        $this->starting_rate = $tariffs->starting_rate;
        $this->town_km_price = $tariffs->town_km_price;
        $this->countryside_km_price = $tariffs->countryside_km_price;
        $this->type_tariff_use = $tariffs->type_tariff_use;
    }

    public function fillTariffs($tariffs)
    {
        $this->town = $tariffs->town;
        $this->town_center = $tariffs->town_center;
        $this->km_price = $tariffs->km_price;
        $this->periphery = $tariffs->periphery;
        $this->type_tariff_use = $tariffs->type_tariff_use;
    }

    public function fillCustomTariff($tariff)
    {
        $this->custom_tariff = $tariff->custom_tariff;
        $this->type_tariff_use = $tariff->type_tariff_use;
        $this->use_custom_tariff = $tariff->use_custom_tariff;
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails())
        {
            $this->fill($request->all());

            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function driver()
    {
        return $this->belongsTo('App\Http\Models\Driver');
    }
    public function brand()
    {
        return $this->belongsTo('App\Http\Models\Brand');
    }

    public function car()
    {
        return $this->belongsTo('App\Http\Models\Car');
    }

    public static function getInfo($id)
    {
        $model = Parking::find($id);
        if (!$model)
        {
            return response()->json($model, 403);
        }
        $model->driver;
        $model->car;
        $model->car->images;
        $model->car->carBrand;
        $model->car->carClass;
        if ($model)
            return response()->json($model, 200);
        else
            return response()->json($model, 403);
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }

    public function order(Request $request)
    {
        $this->order_from = $request->get('order_from');
        $order = new Order();
        $order->fill($request->except('id'));
        $order->zone = $request->get('zone');
        $order->user_id = Auth::user()->id;
        $result = $order->sendMessage([
            'zone_id' => $request->get('zone'),
        ]);
        if ($result == 'succeed')
        {
            $order->save();
            $return = $this;
            $return->driver;
            $return->car;
            $return->brand;
            $return->car->carBrand;
            $return->car->carClass;
            $this->save();
            $this->delete();
            return response()->json($return, 200);
        }
        else
        {
            return response()->json($result, 403);
        }
    }

}
