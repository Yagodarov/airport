<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 13.11.2018
 * Time: 16:44
 */

namespace App\Models\Phone;


use App\Http\Models\Driver;
use App\Http\Models\User\User;
use App\Models\Options;
use App\Models\UserDriver;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PhoneRegister extends Model
{
  public function getToken(Request $request)
  {
    $validator = \Validator::make($request->all(), [
      'phone_number' => ['required', 'min:10', function ($attribute, $value, $fail) use ($request) {
        $driver = Driver::where('phone_number', 'LIKE', "%" . substr($request->get('phone_number'), -9))->first();
        if (!$driver)
          $fail("Такой водитель не найден в базе");
      }]], $this->getMessages());
    if ($validator->fails()) {
      return response()->json($validator->errors()->all(), 403);
    } else {
      $this->sendToken($request);
      return response()->json(true, 200);
    }
  }

  public function sendToken($request)
  {
    $driver = Driver::where('phone_number', 'LIKE', "%" . substr($request->get('phone_number'), -9))->first();
    $smsCode = rand(1000, 9999);
    $driver->sms_token = $smsCode;
    $driver->save();

    $message = "Ваш код: " . $driver->sms_token;

    $soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');
    $result = $soapClient->send_sms([
      'login' => Options::find(1)['sms_login'],
      'psw' => Options::find(1)['sms_password'],
      'phones' => $driver->phone_number,
      'mes' => $message,
      'sender' => '89175566606'
    ]);
  }

  public function changePassword(Request $request)
  {
    $validator = \Validator::make($request->all(), [
      'phone_number' => ['min:9|required', function ($attribute, $value, $fail) use ($request) {
        $driver = Driver::where('phone_number', 'LIKE', "%" . substr($request->get('phone'), -9))
          ->first();

        if (!$driver)
          $fail("Такой водитель не найден в базе");
      }],
      'token' => ['min:4|required', function ($attribute, $value, $fail) use ($request) {
        $driver = Driver::where('phone_number', 'LIKE', "%" . substr($request->get('phone'), -9))
          ->where('sms_token', '=', $request->get('token'))
          ->first();

        if (!$driver)
          $fail("Неправильный токен");
      }],
      'password' => ['min:4', 'required']
    ], $this->getMessages());
    if ($validator->fails()) {
      return response()->json($validator->errors()->all(), 403);
    } else {
        $driver = Driver::where('phone_number', 'LIKE', "%" . substr($request->get('phone'), -9))
          ->where('sms_token', '=', $request->get('token'))->first();
        $userDriver = UserDriver::where('driver_id', '=', $driver->id)->first();
        $user = User::where('id', '=', $userDriver->user_id)->first();
        $user->password = bcrypt($request->get('password'));
        $user->save();
        return response()->json(true, 200);
    }
  }

  public function getMessages()
  {
    return [
      'phone_number.required' => 'Необходимо ввести номер телефона',
      'phone_number.min' => 'Введите правильный номер телефона',
      'password.min' => 'Пароль должен состоять из :min символов',
      'required' => 'Заполните это поле',
      'required_if' => 'Заполните это поле',
      'required_without' => 'Заполните это поле',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }
}
