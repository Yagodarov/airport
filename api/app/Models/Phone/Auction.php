<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 19.11.2018
 * Time: 20:34
 */

namespace App\Models\Phone;


use App\Http\Models\Driver;
use App\Http\Models\GMaps\DistanceMatrix;
use App\Http\Models\PushNotification;
use App\Http\Models\User\User;
use App\Models\AuctionDriver;
use App\Models\Options;
use App\Models\UserDriver;
use Illuminate\Http\Request;

class Auction extends \App\Http\Models\Auction
{
  public $winner;
  public static function search(Request $request) {
    $models = Auction::
      where("status", "=", "1")
      ->when($request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
      })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('auction.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models
      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request) {
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public static function getWonByUser(Request $request)
  {
    $models = Auction::select("auction.*")
      ->leftJoin('auction_driver as ad', 'ad.auction_id', '=', 'auction.id')
      ->leftJoin('user_driver as ud', 'ud.driver_id', '=', 'ad.driver_id')
      ->where('ud.user_id', '=', \Auth::user()->id)
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('auction.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function chooseWinner()
  {
    $length = (new DistanceMatrix([
      'origin' => 'Уфа, международный аэропорт',
      'destinations' => $this->address,
    ]))->getResult();
    if ($length) {
      $length = ($length->rows['0']->elements[0]->distance->value) / 1000;

      $auction_requests = AuctionRequest
        ::where('auction_id', '=', $this->id)
        ->get();
      $lowestPrice = 99999999999;
      $winner = null;
      foreach ($auction_requests as $auction_request) {
        if ($auction_request->km_price && $auction_request->total_price) {
          $price = $auction_request->km_price * $length >= $auction_request->total_price ?
            $auction_request->km_price * $length : $auction_request->total_price;
          if ($price < $lowestPrice) {
            $lowestPrice = $price;
            $winner = $auction_request;
          }
        }
      }
      $this->winner = $winner;
      $this->sendMessages();
    } else {
      $this->status = 2;
      $this->save();
    }
  }

  public function sendMessages()
  {
    $this->pushMessageWinner();
    $this->pushMessageLoser();
  }

  public function setAuctionWinner($driver)
  {

    $auctionDriver = new AuctionDriver();
    $auctionDriver->driver_id = $driver->id;
    $auctionDriver->auction_id = $this->id;
    $auctionDriver->save();
  }

  public function pushMessageWinner()
  {
    $auctionRequest = $this->winner;


    if ($auctionRequest)
    {
      $this->sendWinnerNotification($auctionRequest);
    }

    if ($auctionRequest) {
      $this->sendLoserNotification($auctionRequest);
    }
    if ($auctionRequest) {
      $userDriver = UserDriver::where('user_id', '=', $auctionRequest->user_id)->first();
      $driver = Driver::where('id', '=', $userDriver->driver_id)->first();
      $this->setAuctionWinner($driver);

      $this->car_id = $auctionRequest->car_id;
      $this->driver_id = $userDriver->driver_id;


      if ($phone = $this->client_phone_number) {
        $soapClient = new \SoapClient('https://smsc.ru/sys/soap.php?wsdl');
        $result = $soapClient->send_sms([
          'login' =>  Options::find(1)['sms_login'],
          'psw' =>  Options::find(1)['sms_password'],
          'phones' => $driver->phone_number,
          'mes' => "Номер телефона клиента : ".$phone,
//          'id' => $this->auction->,
          'sender' => '89175566606'
        ]);
        $result = $soapClient->send_sms([
          'login' =>  Options::find(1)['sms_login'],
          'psw' =>  Options::find(1)['sms_password'],
          'phones' => $phone,
          'mes' => "Номер телефона водителя : ".$driver->phone_number,
//          'id' => $this->order_id,
          'sender' => '89175566606'
        ]);
      }
    }

    $this->status = 2;
    $this->save();

  }

  public function sendWinnerNotification($auctionRequest)
  {
    $user = User::find($auctionRequest->user_id);
    $message = 'Вы выиграли в аукционе';
    (new PushNotification())->push(
      $user->push_token,
      $message,
      'Аэропорт УФА',
      [
        'id' => $auctionRequest->auction_id,
        'action' => '1'
      ]
    );
  }

  public function sendLoserNotification($auctionRequest)
  {
    $auctionLoosers = AuctionRequest
      ::where('auction_id', '=', $auctionRequest->auction_id)
      ->where('user_id', '<>', $auctionRequest->user_id)
      ->get();
    if ($auctionLoosers) {
      foreach ($auctionLoosers as $item) {
        $user = User::find($item->user_id);
        if ($user) {
          $message = 'Аукцион завершен';
          (new PushNotification())->push(
            $user->push_token,
            $message,
            'Аэропорт УФА',
            [
              'id' => $auctionRequest->auction_id,
              'action' => '2'
            ]
          );
        }
      }
    }
  }

  public function pushMessageLoser()
  {

  }
}
