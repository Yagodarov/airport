<?php

namespace App\Http\Models\GMaps;

use Illuminate\Database\Eloquent\Model;

class DistanceMatrix extends Model
{
  public $defaultParams = [];
  private $params;
  private $output;

  public function __construct($params)
  {
    $this->params = $params;
  }

  public function getDistanceValue($number = 0)
  {
    return $this->output['rows'][$number]['elements']['distance']['value'];
  }

  public function getDistanceText($number = 0)
  {
    return $this->output['rows'][$number]['elements']['distance']['text'];
  }

  public function getDurationValue($number = 0)
  {
    return $this->output['rows'][$number]['elements']['duration']['value'];
  }

  public function getDurationText($number = 0)
  {
    return $this->output['rows'][$number]['elements']['duration']['text'];
  }

  public function getResult()
  {
    $ch = curl_init();

    // set url
    $url = $this->getUrl();
    curl_setopt($ch, CURLOPT_URL, $this->getUrl());

    //return the transfer as a string
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    // $output contains the output string
    $output = curl_exec($ch);

    $output = json_decode($output);
    if ($output && $output->status == 'OK') {
      $this->output = $output;
      return $output;
    } else {
      return false;
    }
  }

  public function getUrl()
  {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/";
    $url .= "json?units=metric";
    $url .= "&origins=" . str_replace(' ', '+',$this->params['origin']);
    $url .= "&destinations=" . str_replace(' ', '+',$this->params['destinations']);
    $url .= "&key=" . config('maps.googleMapsKey');
    return $url;
  }
}
