<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Storage;
class Sms extends Model
{
  protected $fillable = [
    'status', 'message', 'send_to'
  ];

  public function rules()
  {
    return [];
  }

  public static function search(Request $request)
  {
    $models =  Sms::when($request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
      })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('sms.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    $models = $models

      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function store(Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    }
    else
    {
      $this->fill($request->all());
      if ($result = $this->save())
      {
        return response()->json($result, 200);
      }
      else
        return response()->json($result, 403);
    }
  }

  public function messages() {
    return [
      'required' => 'Заполните это поле',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }
}
