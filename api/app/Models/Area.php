<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
class Area extends Model
{
    public $timestamps = false;
    protected $table = 'areas';

    public static function search(Request $request)
	{
		$models =  DB::table('areas')
			->select('*')
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('areas.id', 'asc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

	public static function store(Request $request)
	{
		if ($result = self::saveAllAreas($request))
		{
			return response()->json($result, 200);
		}
		else
		{
			return response()->json($result, 403);
		}
	}

	public static function saveAllAreas(Request $request)
	{

		$areas = $request->get('areas');
		foreach ($areas as $areaName => $item) {
			if (count($item) > 0)
			{
				Area::where('zone', '=', $areaName)->delete();
				foreach ($item as $id => $coords) {
					$model = new Area();
					$model->zone = $areaName;
					$model->lat = $coords['lat'];
					$model->lng = $coords['lng'];
					$model->save();
				}
			}
		}
		
		return true;
	}
}
