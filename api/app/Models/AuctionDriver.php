<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 08.02.2019
 * Time: 13:09
 */
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuctionDriver extends Model
{
  protected $table = 'auction_driver';

  protected $fillable = [
    'id', 'driver_id', 'auction_id'
  ];

  public function rules(Request $request)
  {
    return [
      'driver_id' => 'required',
      'auction_id' => 'required',
    ];
  }

  public function validate(Request $request)
  {
    return Validator::make($request->all(), $this->rules($request), $this->messages());
  }

  public function store(Request $request)
  {
    $validator = $this->validate($request) ;
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    }
    else
    {
      $this->fill($request->all());
      if ($result = $this->save())
      {
        return response()->json($result, 200);
      }
      else
        return response()->json($result, 403);
    }
  }

  public function storeUpdate(Request $request)
  {
    $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
    if (!$validate->fails())
    {
      $this->fill($request->all());
      if ($result = $this->save())
      {
        return response()->json($result, 200);
      }
      else
      {
        return response()->json($result, 403);
      }
    }
    else
    {
      return response()->json($validate->errors(), 403);
    }
  }

  public function messages() {
    return [
      'required' => 'Заполните это поле',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
    ];
  }

  public function driver()
  {
    return $this->belongsTo('App\Http\Models\Driver');
  }

  public function auction()
  {
    return $this->belongsTo('App\Http\Models\Car');
  }
}
