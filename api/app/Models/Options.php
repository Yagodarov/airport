<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
/**
 * @property int $id
 * @property string $sms_login
 * @property string $sms_password
 * @property string $mail_login
 * @property string $mail_password
 * @property int $terminal_timeout
 * @property string $created_at
 * @property string $updated_at
 */
class Options extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'sms_login', 'sms_password', 'mail_login', 'mail_password', 'terminal_timeout',
        'created_at', 'updated_at', 'phone_feedback', 'email_feedback'];


    public function rules()
    {
        return [];
    }

    public function store(Request $request)
    {
        $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
        if ($validator->fails()) {
            return response()->json($validator->messages(), 403);
        }
        else
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
                return response()->json($result, 403);
        }
    }

    public function storeUpdate(Request $request)
    {
        $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails())
        {
            $this->fill($request->all());
            if ($result = $this->save())
            {
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function messages() {
        return [
            'required' => 'Заполните это поле',
            'min' => 'Не менее :min символа(-ов)',
            'max' => 'Не более :max символа(-ов)',
            'unique' => 'Уже используется',
            'email' => 'Введите правильный формат email',
        ];
    }
}
