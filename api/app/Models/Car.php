<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Models\Parking\Parking;
use App\Http\Models\GMaps\Geocoder;
use App\Http\Models\GMaps\PointLocation;
use Storage;

class Car extends CustomModel
{
  protected static function boot()
  {
    parent::boot();

    static::created(function (Car $model) {
      $report = new Action();
      $report->action_id = 5;
      $report->model_id = 2;
      $report->car_id = $model->id;
      $report->by_user_id = Auth::user()->id;
      $report->save();
    });

    static::updated(function (Car $model) {
      $report = new Action();
      $report->action_id = 6;
      $report->model_id = 2;
      $report->car_id = $model->id;
      $report->by_user_id = Auth::user()->id;
      $report->save();
      if ($model->is_black_list == 1 && $model->getOriginal('is_black_list') == 0) {
        $report = new Action();
        $report->action_id = 9;
        $report->model_id = 2;
        $report->car_id = $model->id;
        $report->by_user_id = Auth::user()->id;
        $report->save();
      }
      else {
        $report = new Action();
        $report->action_id = 10;
        $report->model_id = 2;
        $report->car_id = $model->id;
        $report->by_user_id = Auth::user()->id;
        $report->save();
      }
    });

    static::deleting(function (Car $model) {
      $car_images = CarImage::where('car_id', $model->id)->get();
      foreach ($car_images as $car_image) {
        $car_image->delete();
      }
      File::deleteDirectory($model->path());
    });
  }

  public function path()
  {
    return public_path() . '/images/cars/' . $this->id . '/';
  }

  protected $fillable = [
    'id', 'car_brand_id', 'class_id', 'group_tariff_id', 'client_type_id', 'brand_id', 'model', 'year', 'color', 'number', 'number_of_passengers', 'baby_chair', 'conditioner', 'baggage_count', 'permission_to_drive', 'is_black_list', 'external_baggage', 'license', 'license_requisites', 'use_group_tariff', 'bank_card_payment', 'report_documents'
  ];

  public function rules(Request $request)
  {
    if ($request->isMethod('post')) {
      return [
        'id',
        'car_brand_id' => 'required',
        'class_id' => 'required',
        'file' => 'required_without:image',
        'group_tariff_id' => 'required_if:use_group_tariff,1',
        'client_type_id' => '',
        'brand_id' => '',
        'model' => '',
        'year' => 'required',
        'color' => 'required',
        'number' => 'required|unique:cars,number',
        'number_of_passengers' => 'required',
        'baby_chair',
        'conditioner',
        'baggage_count',
        'permission_to_drive',
        'is_black_list',
        'external_baggage',
        'license',
        'license_requisites',
        'use_group_tariff' => 'required',
        'bank_card_payment' => 'required',
        'report_documents'
      ];
    } else {
      return [
        'id',
        'car_brand_id' => 'required',
        'class_id' => 'required',
        'file' => 'required_without:image',
        'group_tariff_id' => 'required_if:use_group_tariff,1',
        'client_type_id' => '',
        'brand_id' => '',
        'model' => '',
        'year' => 'required',
        'color' => 'required',
        'number' => 'required|unique:cars,number,' . $request->get('id'),
        'number_of_passengers' => 'required',
        'baby_chair',
        'conditioner',
        'baggage_count',
        'permission_to_drive',
        'is_black_list',
        'external_baggage',
        'license',
        'license_requisites',
        'use_group_tariff' => 'required',
        'bank_card_payment' => 'required',
        'report_documents'
      ];
    }

  }

  public static function search(Request $request)
  {
    // abort(403, $request->get('parking') == '1');
    $models = DB::table('cars')
      ->select(
        'cars.*',
        // 'cars.color',
        'car_brands.name as mark',
        // 'cars.model',
        'car_classes.name as class_name'
      // 'cars.number',
      // 'cars.is_black_list',
      // 'cars.permission_to_drive',
      // 'cars.use_group_tariff',
      // 'cars.group_tariff_id'
      )
      ->leftJoin('car_brands', 'cars.car_brand_id', '=', 'car_brands.id')
      ->leftJoin('car_classes', 'cars.class_id', '=', 'car_classes.id')
      ->when($request->get('parking') == '1', function ($models) use ($request) {
        $models
          ->select('parking.id', 'cars.color', 'car_brands.name as mark', 'cars.model', 'car_classes.name as class_name', 'cars.number', 'cars.use_group_tariff', 'cars.group_tariff_id')
          ->whereNotNull('parking.id')
          ->leftJoin('parking', 'parking.car_id', '=', 'cars.id');
      })
      ->when($request->get('text'), function ($models) use ($request) {
        $models
          ->orWhere("cars.model", 'like', "%{$request->get('text')}%")
          ->orWhere("car_brands.name", 'like', "%{$request->get('text')}%")
          ->orWhere("car_classes.name", 'like', "%{$request->get('text')}%")
          ->orWhere("cars.number", 'like', "%{$request->get('text')}%");
      })
      ->when($request->get('parking') == '0', function ($models) use ($request) {
        $models->leftJoin('parking', 'parking.car_id', '=', 'cars.id')
          ->where('cars.is_black_list', '=', '0')
          ->where('cars.permission_to_drive', '=', '1')
          ->whereNull('parking.id');
      })
      ->when($request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
      })
      ->when(!$request->get('orderBy'), function ($models) use ($request) {
        return $models
          ->orderBy('cars.id', 'desc');
      })
      ->groupBy('id');

    $count = $models->get()->count();
    // abort(403, $models->toSql());
    $models = $models
      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request) {
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($this, 200);
      } else
        return response()->json($result, 403);
    }
  }

  public function storeUpdate(Request $request)
  {
    $validate = Validator::make($request->all(), $this->rules($request), $this->messages());
    if (!$validate->fails()) {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($result, 200);
      } else {
        return response()->json($result, 403);
      }
    } else {
      return response()->json($validate->errors(), 403);
    }
  }

  public function saveFile($request)
  {
    $path = public_path() . '/images/cars/' . $this->id . '/';
    if (!File::exists($path)) {
      File::makeDirectory($path, $mode = 0777, true, true);
    }
    if ($request->get('file')) {
      $file = @\Intervention\Image\Facades\Image::make($request->get('file'));
      if ($file) {
        if (File::exists($path . $this->image)) {
          File::delete($path . $this->image);
          $this->image = $this->id . '_' . time() . '.jpg';
        };
        $file->resize(640, null, function ($constraint) {
          $constraint->aspectRatio();
        })->save($path . $this->image);
        $this->save();
      }
    }
  }

  public function saveImageFromUrl($url)
  {
    $path = public_path() . '/images/cars/' . $this->id . '/';
    $newfile = $path . $this->image;
    if (File::exists($path)) {
      File::delete($path);
    } else {
      File::makeDirectory($path, $mode = 0777, true, true);
    }
    if (copy($url, $newfile)) {
      return true;
    } else {
      return false;
    }
  }

  // Search cars for order in terminal
  public function orderSearch(Request $request)
  {
    $tariff = $this->getTariffByAddress($request->get("address"));
    if ($tariff == 'km_price')
    {
      foreach ([3, 2, 1] as $key => $value) {
        $models = Parking
          ::with('car.carClass')
          ->whereHas('car', function ($models) use ($request) {
            $models
              ->when($request->get('report_documents') == 'true', function ($models) use ($request) {
                return $models->where('report_documents', '=', '1');
              })
              ->when($request->get('baby_chair') == 'true', function ($models) use ($request) {
                return $models->where('baby_chair', '=', '1');
              })
              ->when($request->get('external_baggage') == 'true', function ($models) use ($request) {
                return $models->where('external_baggage', '=', '1');
              })
              ->when($request->get('bank_card_payment') == 'true', function ($models) use ($request) {
                return $models->where('bank_card_payment', '=', '1');
              });
          })
          ->whereHas('driver', function ($models) use ($value) {
            return $models->where('terminal_countryside_order_priority', '=', $value);
          })
          ->with('driver')
          ->with('car.carBrand')
          ->with('car.images')
          ->get();
        if (count($models->all()) > 0) {
          break;
        }
      }
    } else {
      $models = Parking
        ::with('car.carClass')
        ->whereHas('car', function ($models) use ($request) {
          $models
            ->when($request->get('report_documents') == 'true', function ($models) use ($request) {
              return $models->where('report_documents', '=', '1');
            })
            ->when($request->get('baby_chair') == 'true', function ($models) use ($request) {
              return $models->where('baby_chair', '=', '1');
            })
            ->when($request->get('external_baggage') == 'true', function ($models) use ($request) {
              return $models->where('external_baggage', '=', '1');
            })
            ->when($request->get('bank_card_payment') == 'true', function ($models) use ($request) {
              return $models->where('bank_card_payment', '=', '1');
            });
        })
        ->with('driver')
        ->with('car.carBrand')
        ->with('car.images')
        ->get();
    }


    $return = [];

    foreach ($models as $key => $model) {
      if ($price = $this->getPriceByTariff($model, $request->get('length'), $tariff)) {
        $model['price'] = $price;
        $return[] = $model;
      }
    }
    if ($return) {
      return response()->json(['models' => $return], 200);
    } else {
      return response()->json(['models' => $return], 403);
    }
  }

  public function getPriceByTariff($model, $length, $tariff)
  {
    if ($model->type_tariff_use == 1 || !$model->type_tariff_use) {
      if ($tariff == 'km_price') {
        return number_format($length / 1000) * intval($model[$tariff]);
      } else {
        return number_format($model[$tariff]);
      }
    } else {
      if ($tariff == 'town' || $tariff == 'town_center') {
        return number_format($length / 1000) * intval($model['town_km_price']) + intval($model['starting_rate']);
      } elseif ($tariff == 'km_price' || $tariff == 'periphery') {
        return number_format($length / 1000) * intval($model['countryside_km_price']) + intval($model['starting_rate']);
      }
    }
    return false;

  }

  public function getTariffByAddress($address)
  {
    $point = (new Geocoder(['address' => $address]))->getCoords();
    $town = DB::table('areas')->where('zone', '=', 'town')->get();
    $town_center = DB::table('areas')->where('zone', '=', 'town_center')->get();
    $periphery = DB::table('areas')->where('zone', '=', 'periphery')->get();
    $km_price = DB::table('areas')->where('zone', '=', 'km_price')->get();
    if ($town_center = (new PointLocation())->pointInPolygon($point, $town_center)) {
      return "town_center";
    }
    if ($town = (new PointLocation())->pointInPolygon($point, $town)) {
      return "town";
    }
    if ($periphery = (new PointLocation())->pointInPolygon($point, $periphery)) {
      return "periphery";
    }
    if ($km_price = (new PointLocation())->pointInPolygon($point, $km_price)) {
      return "km_price";
    }
  }

  public function carBrand()
  {
    return $this->belongsTo('App\Http\Models\CarBrand');
  }

  public function brand()
  {
    return $this->belongsTo('App\Http\Models\Brand');
  }

  public function carClass()
  {
    return $this->belongsTo('App\Http\Models\CarClass', 'class_id', 'id');
  }

  public function images()
  {
    return $this->hasMany('App\Http\Models\CarImage');
  }
}
