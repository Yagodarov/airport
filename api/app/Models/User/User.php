<?php

namespace App\Http\Models\User;

use App\Http\Models\Action;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Facades\Input;
//use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
	use HasRoles;
    use Notifiable;
	protected $guard_name = 'api';
	public $pw;

  protected static function boot()
  {
    parent::boot();

    static::created(/**
     * @param User $model
     */
      function (User $model) {
        if (Auth::user() && Auth::user()->id)
        {
          $report = new Action();
          $report->action_id = 1;
          $report->model_id = 1;
          $report->user_id = $model->id;
          $report->by_user_id = Auth::user()->id;
          $report->save();
        }
    });

    static::updated(function (User $model) {
      if (Auth::user() && Auth::user()->id) {
        $report = new Action();
        $report->action_id = 2;
        $report->model_id = 1;
        $report->user_id = $model->id;
        $report->by_user_id = Auth::user()->id;
        $report->save();

        $x = ($model->getAttribute('blocked') != $model->getOriginal('blocked')) && $model->attributes['blocked'] == 1;
        $status = $model->getOriginal('blocked');
        if (($model->getAttribute('blocked') != $model->getOriginal('blocked'))) {
          if ($model->getAttribute('blocked') == 1) {
            $report = new Action();
            $report->action_id = 3;
            $report->model_id = 1;
            $report->user_id = $model->id;
            $report->by_user_id = Auth::user()->id;
            $report->save();
          } else {
            {
              $report = new Action();
              $report->action_id = 4;
              $report->model_id = 1;
              $report->user_id = $model->id;
              $report->by_user_id = Auth::user()->id;
              $report->save();
            }
          }
        }
      }
    });

    static::deleting(function (User $model) {
    });
  }

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

    protected $fillable = [
        'id', 'username', 'email', 'password', 'blocked', 'first_name', 'surname', 'patronymic',
        'terminal', 'phone_number', 'token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'password'
    ];

	public static function search(Request $request)
	{
		$users =  User::with('roles')
      ->role(['admin', 'manager', 'operator', 'terminal_user'])
      ->leftJoin('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
			->when($request->get('username'), function($users) use ($request){
				return $users->where('users.username', 'LIKE', "%{$request->get('username')}%");
			})
			->when($request->get('full_name'), function($users) use ($request){
				$array = explode(" ", $request->get('full_name'));
				$array = "%".implode("%", $array)."%";
				return $users->whereRaw("CONCAT_WS(' ', users.first_name, users.surname, users.patronymic) LIKE '{$array}'");
			})
			->when($request->get('email'), function($users) use ($request){
				return $users->where('users.email', 'LIKE', "%{$request->get('email')}%");
			})
			->when($request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($users) use ($request) {
				return $users
					->orderBy('users.id', 'desc');
			})
			->when($request->get('blocked') > -1, function ($users) use ($request){
				return $users->where('users.blocked', '=', $request->get('blocked'));
			})
			->when($request->get('role'), function ($users) use ($request){
        return $users->role($request->get('role'));
			  $users->leftJoin('roles', 'model_has_roles.role_id', 'roles.id')
        ->select('users.*');
				return $users->where('roles.name', '=', $request->get('role'));
			})
			->groupBy('users.id');

		$count = $users->get()->count();
		$users = $users

			->when($request->get('page') >= 0 && !$request->get('all'), function ($users) use ($request){
				return $users->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $users,
			'count' => $count
		]);
	}

	public function rules(Request $request)
	{
		if ($request->isMethod('post')) {
			return [
				'username' => 'required|min:4|max:191|unique:users,username',
				'password' => 'nullable|min:4|max:191',
				'first_name' => 'required',
				'surname' => 'required',
        'terminal' => 'required_if:role,manager|required_if:role,admin',
				'email' => 'required|email|unique:users,email',
				'role' => 'required'
			];
		}
		elseif ($request->isMethod('put')) {
			return [
				'username' => 'required|min:4|max:191|unique:users,username,'.$request->get('id'),
				'password' => 'sometimes|min:4|max:191',
				'first_name' => 'required',
				'surname' => 'required',
				'terminal' => 'required_if:role,manager|required_if:role,admin',
				'email' => 'required|email|unique:users,email,'.$request->get('id'),
				// 'email' => 'required|email|unique:users,email,'.$request->get('id'),
				'blocked' => [ 'sometimes', function($attribute, $value, $fail) {
		            if (Auth::user()->id == $this->id && $this->blocked == 1) {
		                return $fail("Нельзя заблокировать себя");
		            }
		        },],
				'role' => [ 'sometimes', function($attribute, $value, $fail) {
		            if (Auth::user()->id == $this->id && $value != $this->getRoleNames()[0]) {
		                return $fail("Нельзя сменить свою роль");
		            }
		        },]
			];
		}
		else {
			abort(403);
		}
	}

	public function store(Request $request)
	{
		$validator = \Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill(Input::all());
			if ($this->password == ''){
                $this->pw = str_random(6);
	            $this->password = bcrypt($this->pw);
            }
            else
            {
                $this->pw = $request->password;
	            $this->password = bcrypt($request->password);
            }
			if ($result = $this->save())
			{
				$profile = Profile::create(['id' => $this->id]);
				$profile->save();
				$this->setRole($request);
				@$this->sendRegistrationEmail($this);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
    {
    	$this->fill(Input::all());
        $validate = \Validator::make($request->all(), $this->rules($request), $this->messages());
        if (!$validate->fails() && $user = \App\Http\Models\User\User::find($request->get('id')))
        {
        	$this::find($request->get('id'));
	        $this->pw = $request->password;
	        if (!$request->password)
	        {
		        $this->password = $this->getOriginal('password');
	        }
	        else
	        {
		        $this->password = bcrypt($request->password);
	        }
	        if ($this->pw || ($this->getOriginal('username') != $this->username))
	        	@$this->sendUpdateUserEmail($this);
            if ($result = $this->save())
            {
            	$this->setRole($request);
                return response()->json($result, 200);
            }
            else
            {
                return response()->json($result, 403);
            }
        }
        else
        {
            return response()->json($validate->errors(), 403);
        }
    }

    public function setRole($request)
    {
    	if ($request->role)
    	{
    		if (count($this->getRoleNames()) > 0)
    			$this->removeRole($this->getRoleNames()[0]);
	        $this->assignRole($request->role);
    	}
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'required_if' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}

    public function sendUpdateUserEmail($user)
    {
	    if ($this->pw)
	    {
		    $messsage = 'Логин: '.$user->username.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
	    }
	    else
	    {
		    $messsage = 'Логин: '.$user->username.' Для сайта '.$this->getUrl();
	    }
        return Mail::raw($messsage, function ($message) use ($user){
            $message->to($user->email);
            $message->subject('Данные для входа на сайт были обновлены');
        });
    }

    public function sendRegistrationEmail($user)
    {
        $messsage = 'Логин: '.$user->username.'. Пароль: '.$this->pw.'.'.' Для сайта '.$this->getUrl();
        return Mail::raw($messsage, function ($message) use ($user){
	            $message->to($user->email);
            $message->subject('Данные для входа на сайт');
        });
    }
    public function getUrl()
    {
    	$url = App::make('url')->to('/');
    	return parse_url($url, PHP_URL_SCHEME).'://'.str_replace('api', '', parse_url($url, PHP_URL_HOST));
    }

    public function getFullName()
    {
      return $this->first_name.' '.$this->surname.' '.$this->patronymic;
    }

    public function driver()
    {
      return $this->belongsToMany('App\Http\Models\Driver', 'user_driver');
    }
}
