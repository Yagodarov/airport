<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Storage;
class DriverTariff extends CustomModel
{
	protected $fillable = [
        'id', 'town_center', 'town', 'periphery', 'use_custom_tariff', 'custom_tariff', 'km_price', 'type_tariff_use', 'starting_rate', 'town_km_price', 'countryside_km_price'
    ];

    public function rules()
    {
    	return [
    		// 'id' => 'required',
    		'town' => 'required_if:type_tariff_use,1',
    		'custom_tariff' => 'required_if:use_custom_tariff,1'
    	];
    }

    public static function search(Request $request)
	{
		$models =  DB::table('driver_tariffs')
			->select('*')
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('driver_tariffs.id', 'desc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models

			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
				return $models->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

	public function validate(Request $request)
	{
		return Validator::make($this->getAttributes(), $this->rules($request), $this->messages());
	}

    public function store(Request $request)
	{

		if ($this->validate()->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save()) {
				return response()->json($result, 200);
			}
			else {
				return response()->json($result, 403);
			}
		}
		else {
			return response()->json($validate->errors(), 403);
		}
	}
}
