<?php

namespace App\Http\Models;

use App\Http\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;

class Action extends CustomModel
{
  protected $fillable = [
    'id', 'user_id', 'action_id', 'model_id'
  ];

  public function rules()
  {
    return [
      'user_id' => 'required',
      'action_id' => 'required',
      'model_id' => 'required',
      'row_id' => 'required'
    ];
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
  public function byUser()
  {
    return $this->belongsTo('App\Http\Models\User\User', 'by_user_id');
  }
  public function driver()
  {
    return $this->belongsTo('App\Http\Models\Driver', 'driver_id');
  }
  public function car()
  {
    return $this->belongsTo('App\Http\Models\Car', 'car_id');
  }

  public static function search(Request $request)
  {
    $models = Action
      ::with('driver')
      ->with('user')
      ->with('byUser')
      ->with('car')
      ->with('car.carBrand')
      ->with('car.carClass')
      ->when($request->get('action_id'), function($models) use ($request) {
        return $models->where('action_id', '=', $request->get('action_id'));
      })
      ->when($request->get('user_id'), function ($models) use ($request) {
        $models->where('by_user_id', '=', $request->get('user_id'));
      });
    $count = $models->get()->count();
    $models = $models
      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->orderBy('id', 'desc')
      ->get();
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function store(Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($result, 200);
      } else
        return response()->json($result, 403);
    }
  }
}
