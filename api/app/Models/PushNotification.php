<?php

namespace App\Http\Models;


use Illuminate\Support\Facades\Auth;
use Sly\NotificationPusher\GcmPushService;
use Sly\NotificationPusher\PushManager;
class PushNotification
{
	public function push($pushToken, $body, $title, $data)
    {
    	$apiKey = \Config::get("push-notification.appNameAndroid")["apiKey"];

        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $fields = [
            'registration_ids' => [$pushToken],
            'data' => $data,
            "notification" => [
                "body" => $body,
                "title" => $title,
                "sound"=> "default"
            ]
        ];
        $headers = [
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        ];

        // Open connection
        $ch = curl_init();

        // Set the URL, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url);
        curl_setopt( $ch, CURLOPT_POST, true);
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode( $fields));

        // Execute post
        $result = curl_exec($ch);

        // Close connection
        curl_close($ch);
        // print the result if you really need to print else neglate thi
        file_put_contents("dump.txt", $result);
        //print_r($result);
        //var_dump($result);
    }
}
