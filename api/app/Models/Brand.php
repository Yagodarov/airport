<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Storage;
class Brand extends Model
{
	protected $fillable = [
        'name', 'image', 'id'
    ];

    protected static function boot()
	{
	    parent::boot();

	    static::deleted(function (Brand $image) {
	        File::deleteDirectory($image->path());
	    });
	}

	public function path()
	{
		return public_path().'/images/brands/'.$this->id.'/';
	}

    public function rules()
    {
    	return [];
    }

    public static function search(Request $request)
	{
		$models =  DB::table('brands')
			->select('*')
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('brands.id', 'desc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models

			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
				return $models->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

    public function store(Request $request)
	{
		$validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				$this->saveFile($request);
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function storeUpdate(Request $request)
	{
		$validate = Validator::make($request->all(), $this->rules($request), $this->messages());
		if (!$validate->fails())
		{
			$this->fill($request->all());
			if ($result = $this->save())
			{
				$this->saveFile($request);
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function saveFile($request)
	{
		$path = public_path().'/images/brands/'.$this->id.'/';
		if (!File::exists($path)) {
			File::makeDirectory($path, $mode = 0777, true, true);
		}
		if ($request->get('file'))
		{
			if (File::exists($path.$this->image))
			{
				File::delete($path.$this->image);
			};
			$this->image = $this->car_id.'_'.time().'.jpg';
			$file = \Intervention\Image\Facades\Image::make($request->get('file'))->resize(640, null, function ($constraint) {
			    $constraint->aspectRatio();
			})->save($path.$this->image);
			$this->save();
		}
	}

	public function saveImage($url)
    {
        $path = public_path().'/images/brands/'.$this->id.'/';
        $newfile = $path.$this->image;
        if (File::exists($path))
        {
            File::delete($path);
        }
        else {
            File::makeDirectory($path, $mode = 0777, true, true);
        }
        if ( copy($url, $newfile) ) {
            return true;
        }else{
            return false;
        }
    }

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}
}
