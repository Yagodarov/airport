<?php

namespace App\Http\Models;

use Illuminate\Support\Facades\App;
use Faker\Provider\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Shift extends CustomModel
{
	protected $table = 'shift';
	protected $fillable = [
        'user_id', 'id', 'status'
    ];

    public function rulesOpen()
    {
    	return [
    		'user_id' => ['required', 'exists:users,id', function($attribute, $value, $fail) {
    			if (Shift::where([
    				['user_id', '=', Auth::user()->id],
    				['status', '=', 1]
    			])->first())
    			{
    				$fail("Смена уже открыта");
    			}
    		}],
    	];
    }

     public function rulesClose()
    {
    	return [
    		'user_id' => ['required', 'exists:users,id', function($attribute, $value, $fail){
    			if (!Shift::where([
    				['user_id', '=', Auth::user()->id],
    				['status', '=', 1]
    			])->first())
    			{
    				$fail("Нет открытой смены");
    			}
    		}],
    	];
    }

    public static function search(Request $request)
	{
		$models =  DB::table('shift')
			->select('*')
			->when($request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy($request->get('orderBy'), $request->get('desc') == 'true' ? 'desc' : 'asc');
			})
			->when(!$request->get('orderBy'), function ($models) use ($request) {
				return $models
					->orderBy('client_types.id', 'desc');
			})
			->groupBy('id');

		$count = $models->get()->count();
		$models = $models

			->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
				return $models->skip($request->get('page') * 10)->take(10);
			})
			->get();
		return response()->json([
			'models' => $models,
			'count' => $count
		]);
	}

    public function start(Request $request)
	{
		$validator = Validator::make(['user_id' => Auth::user()->id], $this->rulesOpen($request), $this->messages());
		if ($validator->fails()) {
			return response()->json($validator->messages(), 403);
		}
		else
		{
			$this->user_id = Auth::user()->id;
			if ($result = $this->save())
			{
				return response()->json($result, 200);
			}
			else
				return response()->json($result, 403);
		}
	}

	public function close(Request $request)
	{
		$validate = Validator::make(['user_id' => Auth::user()->id], $this->rulesClose($request), $this->messages());
		if (!$validate->fails())
		{
			$shift = Shift::where("user_id", "=", Auth::user()->id)->first();
			$shift->status = 0;
			if ($result = $shift->save())
			{
				return response()->json($result, 200);
			}
			else
			{
				return response()->json($result, 403);
			}
		}
		else
		{
			return response()->json($validate->errors(), 403);
		}
	}

	public function messages() {
		return [
			'required' => 'Заполните это поле',
			'min' => 'Не менее :min символа(-ов)',
			'max' => 'Не более :max символа(-ов)',
			'unique' => 'Уже используется',
			'email' => 'Введите правильный формат email',
		];
	}
}
