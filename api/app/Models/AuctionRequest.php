<?php

namespace App\Http\Models;

use App\Http\Models\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Validator;

class AuctionRequest extends CustomModel
{


    public function user()
  {
    return $this->belongsTo(Auction::class);
  }
  public function byUser()
  {
    return $this->belongsTo('App\Http\Models\User\User', 'by_user_id');
  }
  public function driver()
  {
    return $this->belongsTo('App\Http\Models\Driver', 'driver_id');
  }
  public function car()
  {
    return $this->belongsTo('App\Http\Models\Car', 'car_id');
  }

  public static function search(Request $request)
  {
    $models = Action
      ::with('driver')
      ->with('user')
      ->with('byUser')
      ->with('car')
      ->with('car.carBrand')
      ->with('car.carClass')
      ->when($request->get('action_id'), function($models) use ($request) {
        return $models->where('action_id', '=', $request->get('action_id'));
      })
      ->when($request->get('user_id'), function ($models) use ($request) {
        $models->where('by_user_id', '=', $request->get('user_id'));
      });
    $count = $models->get()->count();
    $models = $models
      ->when($request->get('page') >= 0 && !$request->get('all'), function ($models) use ($request){
        return $models->skip($request->get('page') * 10)->take(10);
      })
      ->orderBy('id', 'desc')
      ->get();
//    foreach ($models as $model)
//    {
//      if ($model->by_user_id) {
//        $model->by_user = User::find($model->by_user_id);
//      }
//    }
    return response()->json([
      'models' => $models,
      'count' => $count
    ]);
  }

  public function store(Request $request)
  {
    $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json($validator->messages(), 403);
    } else {
      $this->fill($request->all());
      if ($result = $this->save()) {
        $this->saveFile($request);
        return response()->json($result, 200);
      } else
        return response()->json($result, 403);
    }
  }
}
