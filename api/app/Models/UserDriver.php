<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 13.11.2018
 * Time: 21:58
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UserDriver extends Model
{
  protected $table = 'user_driver';

  protected $fillable = [
    'driver_id', 'user_id'];

  public $timestamps = false;
}
