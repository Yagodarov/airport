<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Parking\Parking as BaseModel;
use App\Http\Models\Parking\ParkingMifare;

class ParkingController extends Controller
{
    public function index(Request $request)
    {
        return BaseModel::search($request);
    }

    public function order(Request $request)
    {
         $model = BaseModel::find($request->get('id'));
         return $model->order($request);
    }

    public function addByMifareCardId(Request $request)
    {
        $model = new ParkingMifare();
        return response()->json($model->addByMifareCardId($request));
    }

    public function getFullInfo(Request $request)
    {
        $model = new ParkingMifare();
        return response()->json($model->getFullInfo($request));
    }

    public function get($id)
    {
        $model = BaseModel::find($id);
        return response()->json($model);
    }

    public function getInfo($id)
    {
        return BaseModel::getInfo($id);
    }

    public function store(Request $request)
    {
        return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
        return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
        $model = BaseModel::find($id);
        if ($model)
        {
            return response()->json($model->delete(), 200);
        } else
        {
            return response()->json([
                'messages' => 'Уже удален'
            ], 200);
        }
    }
}
