<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\DriverTariff as BaseModel;
class DriverTariffController extends Controller
{
  //   public function index(Request $request)
  //   {
		// return BaseModel::search($request);
  //   }

	public function get($id)
	{
		$model = BaseModel::find($id);
        if (!$model)
        {
            $model = new BaseModel();
            $model->id = $id;
            $model->save();
        }
		return response()->json($model);
	}

    public function store(Request $request)
    {
	    return (new BaseModel())->store($request);
    }

    public function update(Request $request)
    {
        $model = BaseModel::find($request->get('id'));
	    return ($model->storeUpdate($request));
    }

    public function delete($id)
    {
	    return response()->json(BaseModel::find($id)->delete());
    }
}
