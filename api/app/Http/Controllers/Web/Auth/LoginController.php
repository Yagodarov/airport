<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Models\Action;
use App\Http\Models\User\User;
use App\Http\Models\User\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
	public function authenticate(Request $request)
	{
		// grab credentials from the request
		$credentials = $request->only('username', 'password');
		try {
			$user = User::where('username', $request->get('username'))->first();
			if ($user && $user->blocked)
			{
				return response()->json('Аккаунт заблокирован', 401);
			}
			if (! $token = \JWTAuth::attempt($credentials)) {
				return response()->json('Неверные данные', 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json('Ошибка сервера', 500);
		}

		// all good so return the token
		$permissions = $user->getAllPermissions()->pluck('name');
		$role = $user->getRoleNames()[0];

		$actionUser = new Action();
		$actionUser->model_id = 1;


		return response()->json([
			'token' => $token,
			'permissions' => $permissions,
			'role' => $role,
			'user' => [
				'id' => $user['id'],
				'username' => $user['username']
			],
		]);
	}

	public function logout()
  {
    if ($user = Auth::getUser()) {
      return response()->json(\JWTAuth::invalidate(\JWTAuth::getToken()));
    }
    return response()->json($user);
  }
}
