<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Area as BaseModel;
class AreaController extends Controller
{
    public function index(Request $request)
    {
		return BaseModel::search($request);
    }

	// public function get($id)
	// {
	// 	$model = BaseModel::find($id);
	// 	return response()->json($model);
	// }

    public function store(Request $request)
    {
	    return BaseModel::store($request);
    }

    // public function update(Request $request)
    // {
    //     $model = BaseModel::find($request->get('id'));
	   //  return ($model->storeUpdate($request));
    // }

    // public function delete($id)
    // {
	   //  return response()->json(BaseModel::find($id)->delete());
    // }
}
