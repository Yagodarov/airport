<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 19.11.2018
 * Time: 20:37
 */

namespace App\Http\Controllers\Phone\v1;


use App\Http\Controllers\Controller;
use App\Models\Phone\Auction;
use App\Models\Phone\Tariff;
use Illuminate\Http\Request;

class TariffController extends Controller
{
  public function get(Request $request)
  {
    return Tariff::getTariff($request->get('id'));
  }

  public function update(Request $request)
  {
    if ($model = Tariff::find($request->get('id'))) {
      return ($model->store($request));
    } else {
      return response()->json([
        'Не найден тариф'
      ], 403);
    }
  }
}
