<?php

namespace App\Http\Controllers\Phone\v1;

use App\Http\Controllers\Controller;
use App\Models\Phone\AuctionRequest;
use Illuminate\Http\Request;

class AuctionRequestController extends Controller
{
  public function bid(Request $request)
  {
    $model = (new AuctionRequest());
    return $model->store($request);
  }
}
