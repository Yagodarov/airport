<?php

namespace App\Http\Controllers\Phone\v1;

use App\Http\Controllers\Controller;
use App\Http\Models\Action;
use App\Http\Models\Driver;
use App\Http\Models\User\User;
use App\Http\Models\User\Profile;
use App\Models\UserDriver;
use App\PhoneRegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
	public function login(Request $request)
	{

    $validator = Validator::make(Input::all(), $this->rules($request), $this->messages());
    if ($validator->fails()) {
      return response()->json(
        $validator->errors()->all(), 403);
    } else {
      $driver = Driver
      ::where('phone_number', 'LIKE', "%".substr($request->get('phone_number'), -9))
      ->first();
      $userDriver = UserDriver::where('driver_id', '=', $driver->id)->first();
      $user = User::find($userDriver->user_id);
      $user->push_token = $request->get('push_token');
      $user->save();

      \Config::set('jwt.user', 'App\Http\Models\Driver');
      \Config::set('auth.providers.users.model', \App\Http\Models\Driver::class);
      $credentials = $request->only('phone_number', 'password');
      $token = \JWTAuth::fromUser($user);

    }


		return response()->json([
			'token' => $token,
			'user' => $driver,
		]);
	}

	public function rules($request){
	  return [
	    'phone_number' => ['required', function($attribute, $value, $fail) use ($request) {

      }],
      'password' => ['required', function($attribute, $value, $fail) use ($request) {
        if ($phoneRegisterRequest = PhoneRegisterRequest::where('phone_number', 'LIKE', "%".substr($request->get('phone_number'), -9))->first())
          if ( $phoneRegisterRequest->status == 0)
            return $fail("Ваш аккаунт не подтвержден администратором");
        if (!$driver = Driver
          ::where('phone_number', 'LIKE', "%".substr($request->get('phone_number'), -9))
          ->first())
          return $fail("Водитель с таким номером не найден");
        if (!$userDriver = UserDriver::where('driver_id', '=', $driver->id)->first())
        {
          return $fail("Ваш аккаунт не подтвержден администратором");
        }
        $user = User::where('id', '=', $userDriver->user_id)->first();
          if (!Hash::check($value, $user->password))
          {
            return $fail('Неправильный пароль');
          }
      }],
      'push_token' => 'required'
    ];
  }

  public function messages() {
    return [
      'required' => 'Заполните :attribute',
      'password.required' => 'Заполните пароль',
      'code.required' => 'Заполните смс код',
      'push_token.required' => 'Заполните пуш токен',
      'min' => 'Не менее :min символа(-ов)',
      'max' => 'Не более :max символа(-ов)',
      'unique' => 'Уже используется',
      'email' => 'Введите правильный формат email',
      'date' => 'Выберите дату',
      'numeric' => 'Заполните это поле'
    ];
  }

	public function logout()
  {
    if ($user = Auth::getUser()) {
      return response()->json(\JWTAuth::invalidate(\JWTAuth::getToken()));
    }
    return response()->json($user);
  }
}
